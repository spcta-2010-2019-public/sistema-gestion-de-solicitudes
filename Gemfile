source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.6'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

gem 'tinymce-rails'

gem 'devise'

gem "recaptcha", require: "recaptcha/rails"

gem "haml-rails", "~> 1.0"

# Complete Ruby geocoding solution.
gem 'geocoder'
# Ckeditor integration gem for rails
gem 'ckeditor'

gem 'rack-cors', :require => 'rack/cors'
gem 'daemons'
gem 'delayed_job_active_record'

gem 'jwt'

# Easy file attachment management for ActiveRecord
gem 'paperclip', '~> 5.1.0'
gem 'angularjs-rails', '~> 1.6.2'

gem 'pg_search', '~> 2.0.1'
gem 'paper_trail', '~> 7.0.2'
gem 'prawn', '~> 2.2.2'
gem 'prawn-table', '~> 0.2.2'
gem 'draper', '~> 3.0.0'
gem 'breadcrumbs_on_rails', '~> 3.0.1'
gem 'ransack', '~> 1.8.2'
gem 'cocoon', '~> 1.2.10'

gem 'will_paginate', '~> 3.1.5'
gem 'cancancan', '~> 2.0'
gem 'rolify'
gem 'date_validator', '~> 0.9.0'

gem "simple_calendar", "~> 2.0"

gem 'acts-as-taggable-on'

#gem 'simple_token_authentication', '~> 1.12'
# ajax upload files
gem 'remotipart'

gem 'jquery-ui-rails'

gem 'rubyzip'

gem 'business_time'

gem 'whenever', require: false

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  # Remote multi-server automation tool
  gem 'capistrano', '3.5'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-ssh-doctor', '~> 1.0'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'rubocop', '~> 0.44.1'
end

gem 'mysql2', '~> 0.4.6'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
