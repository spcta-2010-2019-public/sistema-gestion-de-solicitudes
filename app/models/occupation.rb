class Occupation < ApplicationRecord

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

end
