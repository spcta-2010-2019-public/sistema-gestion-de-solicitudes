class InformationRequestIaipProcessNote < ApplicationRecord

  belongs_to :information_request_iaip_process

  validates :content, presence: true

end
