class InformationRequestIaipProcess < ApplicationRecord
  belongs_to :information_request

  has_many :attachments, foreign_key: "information_request_iaip_process_id", class_name: "InformationRequestIaipProcessAttachment", dependent: :destroy
  has_many :notes, foreign_key: "information_request_iaip_process_id", class_name: "InformationRequestIaipProcessNote", dependent: :destroy

  accepts_nested_attributes_for :notes, allow_destroy: true
  accepts_nested_attributes_for :attachments, allow_destroy: true

  enum process_type: [:appeal, :personal_data, :complaint, :no_response, :informal]

  enum resolution_result: [:confirmatory, :modifying, :revocation, :impermissible, :improbability, :inadmissibility, :dismissal, :withdrawal]

  enum iaip_resolution: [ :confirm, :revoke, :modify, :sanction, :override ]

  after_create :update_information_request_iaip_process

  # Validations
  validates :presentation_date, :notification_date, :correlative, :process_type,
            :description, presence: true

  def update_information_request_iaip_process
    information_request = self.information_request
    information_request.iaip_process_kind = :in_iaip
    information_request.save!(validate: false)
  end



end
