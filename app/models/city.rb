class City < ApplicationRecord

  belongs_to :state

  validates :state_id, :name, :presence => true

  def full_name
    return "#{state.name} / #{name}"
  end

end
