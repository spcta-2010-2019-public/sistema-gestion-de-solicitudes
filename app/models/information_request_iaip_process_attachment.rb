class InformationRequestIaipProcessAttachment < ApplicationRecord
  belongs_to :information_request_iaip_process

  has_attached_file :file
  
  validates :title, presence: true
  validates_attachment :file, presence: true

  do_not_validate_attachment_file_type :file


end
