# frozen_string_literal: true
#
class Institution < ApplicationRecord
  #
  has_and_belongs_to_many :users
  belongs_to :institution_type, foreign_key: :institution_type_id

  has_many :oir_addresses, dependent: :destroy
  has_many :information_requests
  has_many :institution_schedules, dependent: :destroy
  has_one  :institution_calendar, dependent: :destroy
  has_many :calendar_events, through: :institution_calendar

  scope :is_enabled, -> {where(enabled: true)}

  default_scope { order(acts_as_label) }


  after_create :create_calendar

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def create_calendar
    calendar = InstitutionCalendar.new
    calendar.name = "Calendario"
    calendar.institution_id = self.id
    calendar.save!
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def next_correlative
    update_column(
      :information_request_correlative,
      information_request_correlative.to_i + 1
    )
  end

  def next_correlative_personal_request
    update_column(
      :information_request_personal_correlative,
      information_request_personal_correlative.to_i + 1
    )
  end

  def work_date?(d)
    return false if calendar_events.holiday_dates.where(event_date: d).exists?
    return true  if calendar_events.work_dates.where(event_date: d).exists?

    institution_schedules.pluck(:wday).include?(d.cwday)
  end

  def date_schedule_for(date = Date.today)
    institution_schedules.where(wday: date.wday).first
  end

  def business_time?(datetime = Time.zone.now)
    schedule = institution_schedules.where(wday: datetime.wday).first
    return false if schedule.nil?
    schedule.to_time_greater_than?(datetime)
  end

=begin
  def self.times
    h = []
    start_time = Time.now.beginning_of_day + 6.hours
    end_time = start_time + 12.hours
    while start_time <= end_time
      h << start_time.strftime('%H:%M')
      start_time += 30.minutes
    end
    return h
  end

  # Business time config
  def set_business_config
    s = settings.get_all
    BusinessTime::Config.work_hours = work_hours(s)
    BusinessTime::Config.work_week = work_hours(s).keys
    holidays(s).each{|h| BusinessTime::Config.holidays << h }
  end
=end

end
