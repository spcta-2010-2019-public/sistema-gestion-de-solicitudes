class Requirement < ApplicationRecord
  belongs_to :information_request
  has_many :comments, dependent: :destroy
  has_many :events, as: :eventable, dependent: :destroy

  accepts_nested_attributes_for :comments
  accepts_nested_attributes_for :events, reject_if: proc { |attributes| attributes['justification'].blank? }

  validates :requested_information, presence: true

  has_attached_file :file

  do_not_validate_attachment_file_type :file

  delegate :process_days, to: :information_request, prefix: false, allow_nil: true

  enum state: [:newer, :process, :closed]

  after_create :set_state

  def set_state
    self.update_column("state", "newer")
  end

  default_scope { order("id ASC") }

  scope :in_process, -> { where(:state => 1) }
  scope :are_closed, -> { where(:state => 2) }

  def requirements_with_assigned_users
    self.where.not("assigned_users = '{}'")
  end

  def has_assigned_users?
    assigned_users.present?
  end

  def show_assigned_users_name
    User.where("id IN (?)", assigned_users.map(&:to_i)).map(&:name).join(', ')
  end

  def show_assigned_users
    User.where("id IN (?)", assigned_users.map(&:to_i)).map(&:email).join(', ')
  end

  def show_invited_users
    User.where("id IN (?)", invited_users.map(&:to_i)).map(&:email).join(', ')
  end

  def show_invited_users_name
    User.where("id IN (?)", invited_users.map(&:to_i)).map(&:name).join(', ')
  end


  def get_information_type
    InformationRequest::INFORMATION_TYPE[information_type]
  end

  def get_resolution_type
    return '' if self.resolution_type.nil?
    I18n.t self.resolution_type, scope: 'activerecord.enum.information_request.resolution_type' rescue ''
  end
=begin
  RESOLUTION_TYPE = {
    INFORMATION_TYPE['public'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.informal'), 'informal'],
      [I18n.t('activerecord.enum.information_request.resolution_type.public_information'), 'public_information'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_b_laip'), 'art_74_b_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']
    ],
    INFORMATION_TYPE['reserved'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_19_laip'), 'art_19_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.reserved_public_delivery'), 'reserved_public_delivery']
      #[I18n.t('activerecord.enum.information_request.resolution_type.art_30_laip_reserved'), 'art_30_laip_reserved']
    ],
    INFORMATION_TYPE['confidential'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_24_laip'), 'art_24_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.confidential_public_delivery'), 'confidential_public_delivery'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.art_30_laip_confidential'), 'art_30_laip_confidential'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_31_32_laip'), 'art_31_32_laip']
    ],
    INFORMATION_TYPE['non_existent'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_73_laip'), 'art_73_laip']
    ],
    INFORMATION_TYPE['incompetent'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_68_laip'), 'art_68_laip']
    ],
    INFORMATION_TYPE['dismissed'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.not_corrected'), 'not_corrected'],
      [I18n.t('activerecord.enum.information_request.resolution_type.user_abandoned_process'), 'user_abandoned_process']
    ],
    INFORMATION_TYPE['unreasonable'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_a_laip'), 'art_74_a_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_c_laip'), 'art_74_c_laip'],
    ]
  }

  information_type:
    public: Pública
    reserved: Reservada
    confidential: Confidencial
    non_existent: Inexistente
    incompetent: Incompetente
    dismissed: Desestimada
    unreasonable: Improcedente
  resolution_type:
    informal: Oficiosa
    public_information: Pública
    art_74_b_laip: Sin trámite por corresponder al Art. 74-B de la LAIP
    no_administrative_unit_answer: Unidad administrativa no remitió información solicitada
    art_19_laip: Denegada por corresponder al Art. 19 de la LAIP
    reserved_public_delivery: Entrega en versión pública
    art_24_laip: Denegada por corresponder al Art. 24 de la LAIP
    confidential_public_delivery: Entrega en versión pública
    art_31_32_laip: Entrega por ser datos personales y corresponder a los Art. 31 y 32 de la LAIP
    art_73_laip: Inexistente por corresponder al Art. 73 de la LAIP
    art_68_laip: Cerrado por corresponder al Art. 68 de la LAIP
    not_corrected: No subsanado
    user_abandoned_process: La persona solicitó, por escrito, abandonar el trámite
    art_74_a_laip: Sin trámite por corresponder al Art. 74-A de la LAIP
    art_74_c_laip: Sin trámite por corresponder al Art. 74-C de la LAIP

=end
  def complete?
    information_type=='public' or (information_type=='confidential' and resolution_type=='art_31_32_laip')
  end

  def partial?
    (information_type=='reserved' and resolution_type=='reserved_public_delivery') or (information_type=='confidential' and resolution_type=='confidential_public_delivery')
  end

  def not_appropiate?
    information_type=='re_routed' or information_type=='non_existent' or information_type=='undelivered'
  end

  def total_reserve?
    (information_type=='reserved' and resolution_type=='art_19_laip') or (information_type=='confidential' and resolution_type=='art_24_laip')
  end

  def not_processed?
    information_type=='not_processed'
  end

  #def informal?
  #  information_type=='informal'
  #end

  def public?
    information_type=='public'
  end

  def denied_confidential?
    resolution_type=='art_24_laip'
  end

  def public_confidential?
    resolution_type=='confidential_public_delivery'
  end

  def personal_data?
    resolution_type=='art_31_32_laip'
  end

  def denied_reserved?
    resolution_type=='art_19_laip'
  end

  def public_reserved?
    resolution_type=='reserved_public_delivery'
  end

  def public_version?
    resolution_type=='reserved_public_delivery'
  end

  def non_existent?
    information_type=='non_existent'
  end

  def undelivered?
    information_type=='unreasonable'
  end

  def incompetent?
    information_type=='incompetent'
  end

  #def redirected?
  #  information_type=='re_routed'
  #end

  def unprocessed?
    information_type=='not_processed'
  end


end
