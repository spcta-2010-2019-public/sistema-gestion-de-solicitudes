class Event < ApplicationRecord
  belongs_to :user
  belongs_to :eventable, polymorphic: true

  validates :justification, :event_start, :user_id, presence: true

  has_attached_file :document

  do_not_validate_attachment_file_type :document

end
