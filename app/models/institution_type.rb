class InstitutionType < ApplicationRecord
  has_many :institutions
end
