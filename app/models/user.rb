class User < ApplicationRecord
  # acts_as_token_authenticatable
  # Include default devise modules.

  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :trackable
         # :validatable,
         # :confirmable,
         # :lockable

  enum role: [:admin, :oir, :administrative_unit]

  scope :without_admin_and_oir_role, -> { joins(:roles).where("roles.name != ? AND roles.name != ?", "admin", "oir") }
  scope :without_admin_role, -> { joins(:roles).where("roles.name != ?", "admin") }

  has_and_belongs_to_many :institutions
  has_many :comments

  has_one :admin_setting

  after_create :ensure_authentication_token

  validates :name, presence: true

  validates :email, presence: true
  validates :email,
            uniqueness: { case_sensitive: false },
            if: proc { |o| o.email.present? }

  validates :email,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            if: proc { |o| o.email.present? }

  validates :password,
            presence: true,
            confirmation: true,
            on: :create

  validates :password,
            presence: true,
            confirmation: true,
            if: proc { |o| o.password.present? },
            on: :update

  def one_institution?
    institutions.count == 1
  end

  def uniq_institution
    return nil unless one_institution?
    institutions.try(:first)
  end

  def admin?
    self.has_role?("admin")
  end

  def oir?
    self.has_role?("oir")
  end

  def administrative_unit?
    self.has_role?("administrative_unit")
  end


  def users_array
    if admin?
      my_users = User.order(:email)
    else
      my_users = User.where("id IN (?)", Institution.find(self.institution_ids.first).user_ids).where("id != ?", self.id)
    end
    my_users.map{ |user| [user.name, user.id] }
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
      self.save!
    end
  end

  private

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

end
