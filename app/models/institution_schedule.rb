# frozen_string_literal: true
#
class InstitutionSchedule < ApplicationRecord
  include PgSearch

  belongs_to :institution

  validates :wday, presence: true
  validates :wday, uniqueness: { scope: :institution_id }

  validates :from, presence: true

  validates :to, presence: true
  validates :to,
            date: {
              after: proc { |o| o.from },
              message: 'Debe ser mayor que "Desde"'
            }

  after_save :update_requests_deadline

  def to_time_greater_than?(datetime)
    return true if to.hour > datetime.hour
    (to.hour == datetime.hour && to.min >= datetime.min)
  end

  def update_requests_deadline
    requests = InformationRequest.where(institution_id: self.institution_id).are_admitted.are_not_closed
    if requests.size > 0
      requests.each do |request|
        request.update_delivery_deadline
      end
    end
  end

end
