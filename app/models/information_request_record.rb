class InformationRequestRecord < ApplicationRecord
    belongs_to :institution
    belongs_to :user

    validates :institution_id, :total_requirements, :dismissed_requirements, :abandoned_requirements, :opening_date, presence: true
    validates :closing_date, :total_requests, presence: true
    validates :art_10_17_laip, :no_proprietary, :art_31_32_laip, :art_30_laip, presence: true
    validates :art_19_laip, :art_24_laip, :art_73_laip, :art_67_laip, :process_requirements, :art_74_laip, presence: true
    validates :dismissed_requirements, :abandoned_requirements, :process_requirements, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :total_requirements, :total_requests, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :art_10_17_laip, :no_proprietary, :art_31_32_laip, :art_30_laip, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validates :art_19_laip, :art_24_laip, :art_73_laip, :art_67_laip, :art_74_laip, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validate :opening_date_less_than_closing_date
    #validate :requirements_greather_than_requests

    # Methods
    def opening_date_less_than_closing_date
      if opening_date.blank? or closing_date.blank? or opening_date > closing_date
        errors.add :opening_date, I18n.t('activerecord.errors.messages.information_request_records.less_than_closing_date')
      end
    end
    def requirements_greather_than_requests
      if total_requirements.to_i < total_requests.to_i
        errors.add :total_requests, I18n.t('activerecord.errors.messages.information_request_records.less_than_total_requirements')
      end
    end

    def public_version
      art_30_laip
    end

    def not_appropiate
      dismissed_requirements + art_73_laip + art_67_laip + art_74_laip
    end

    def valid_totals?
      total_requirements >= total_requests
    end

    def valid_requirements?
      total_requirements == (total_deliveries + process_requirements)
    end

    def total_deliveries
      art_10_17_laip + no_proprietary + art_31_32_laip + art_30_laip + art_19_laip + art_24_laip + art_73_laip + art_67_laip + dismissed_requirements + art_74_laip
    end


end
