class InformationRequest < ApplicationRecord
  include PgSearch
  require 'jwt'

  acts_as_taggable_on :tags
  # serialize
  serialize :notification_mode
  serialize :type_of_rights

  EXTENSION_TYPE = {
    'none' => I18n.t('activerecord.enum.information_request.extension_type.none'),
    'antiquity' => I18n.t('activerecord.enum.information_request.extension_type.antiquity')
  }

  REJECT = {
    'duplicated' => I18n.t('activerecord.enum.information_request.information_type_reject.duplicated'),
    'consultation' => I18n.t('activerecord.enum.information_request.information_type_reject.consultation'),
  }

  INFORMATION_TYPE = {
    'public' => I18n.t('activerecord.enum.information_request.information_type.public'),
    'reserved' => I18n.t('activerecord.enum.information_request.information_type.reserved'),
    'confidential' => I18n.t('activerecord.enum.information_request.information_type.confidential'),
    'non_existent' => I18n.t('activerecord.enum.information_request.information_type.non_existent'),
    'incompetent' => I18n.t('activerecord.enum.information_request.information_type.incompetent'),
    'dismissed' => I18n.t('activerecord.enum.information_request.information_type.dismissed'),
    'unreasonable' => I18n.t('activerecord.enum.information_request.information_type.unreasonable'),
    'unclassified' => I18n.t('activerecord.enum.information_request.information_type.unclassified')
  }
  RESOLUTION_TYPE = {
    INFORMATION_TYPE['public'] => [

      #[I18n.t('activerecord.enum.information_request.resolution_type.public_delivery'), 'public_delivery'],
      [I18n.t('activerecord.enum.information_request.resolution_type.public_information'), 'public_information'],
      [I18n.t('activerecord.enum.information_request.resolution_type.informal'), 'informal'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_b_laip'), 'art_74_b_laip'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']
    ],
    INFORMATION_TYPE['reserved'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_19_laip'), 'art_19_laip'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.reserved_public_delivery'), 'reserved_public_delivery']
      [I18n.t('activerecord.enum.information_request.resolution_type.art_30_laip_reserved'), 'art_30_laip_reserved'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']

    ],
    INFORMATION_TYPE['confidential'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_24_laip'), 'art_24_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.confidential_public_delivery'), 'confidential_public_delivery'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.art_30_laip_confidential'), 'art_30_laip_confidential'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_31_32_laip'), 'art_31_32_laip'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']

    ],
    INFORMATION_TYPE['non_existent'] => [
      #[I18n.t('activerecord.enum.information_request.resolution_type.art_73_laip'), 'art_73_laip']
      [I18n.t('activerecord.enum.information_request.resolution_type.not_generated'), 'not_generated'],
      [I18n.t('activerecord.enum.information_request.resolution_type.not_available'), 'not_available'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']

    ],
    INFORMATION_TYPE['incompetent'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_68_laip'), 'art_68_laip'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']

    ],
    INFORMATION_TYPE['dismissed'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.not_corrected'), 'not_corrected'],
      [I18n.t('activerecord.enum.information_request.resolution_type.user_abandoned_process'), 'user_abandoned_process'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']

    ],
    INFORMATION_TYPE['unreasonable'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_a_laip'), 'art_74_a_laip'],
      [I18n.t('activerecord.enum.information_request.resolution_type.art_74_c_laip'), 'art_74_c_laip'],
      #[I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer']
    ],
    INFORMATION_TYPE['unclassified'] => [
      [I18n.t('activerecord.enum.information_request.resolution_type.no_administrative_unit_answer'), 'no_administrative_unit_answer'],
    ]
  }
  NOTIFICATION_MODE = {
    'email' => I18n.t('activerecord.enum.information_request.notification_mode.email'),
    'phone' => I18n.t('activerecord.enum.information_request.notification_mode.phone'),
    'cellular' => I18n.t('activerecord.enum.information_request.notification_mode.cellular'),
    'fax' => I18n.t('activerecord.enum.information_request.notification_mode.fax'),
    'mail' => I18n.t('activerecord.enum.information_request.notification_mode.mail'),
    'oir_uaip' => I18n.t('activerecord.enum.information_request.notification_mode.oir_uaip'),
  }

  TYPES_OF_RIGHTS = {
    'access' => I18n.t('activerecord.enum.information_request.type_of_rights.access'),
    'rectification'  => I18n.t('activerecord.enum.information_request.type_of_rights.rectification'),
    'cancellation' => I18n.t('activerecord.enum.information_request.type_of_rights.cancellation'),
    'opposition' => I18n.t('activerecord.enum.information_request.type_of_rights.opposition'),
  }

  CORRECT_BUSINESS_DAYS = 5

  # Accessors
  attr_accessor :state_id, :terms

  # Associations
  belongs_to :institution
  belongs_to :city
  belongs_to :occupation

  has_many :requirements, dependent: :destroy
  has_many :events, as: :eventable, dependent: :destroy
  #has_many :comments, as: :commentable, dependent: :destroy
  #has_many :events, as: :eventable, class_name: "InformationRequestEvent", dependent: :destroy

  has_one :iaip_process, class_name: "InformationRequestIaipProcess", dependent: :destroy


  accepts_nested_attributes_for :requirements, allow_destroy: true
  accepts_nested_attributes_for :events

  # Paperclip
  has_attached_file :document_front_image
  has_attached_file :document_back_image
  has_attached_file :legal_representative_doc
  has_attached_file :other_document

  # Validations
  validates :residence, :delivery_way, :entity_type, :firstname, :gender,
            :lastname, :nationality, :occupation_id, :notification_mode,
            :institution_id, :phone, presence: true

  validates :email, presence: true, format: { with: Devise.email_regexp }, confirmation: true, if: Proc.new{|o| o.from_website?}
  validates :educational_level, presence: true, if: Proc.new{|o| o.natural?}
  validates :birthday, presence: true, if: Proc.new{|o| o.natural?}
  #validates :address, presence: true#, if: Proc.new{|o| o.delivery_way_mail? or o.notification_mode_mail?}
  validates :phone, :cellular, :fax, length: { maximum: 200 }
  validates :city_id, :state_id, presence: true, if: Proc.new{|o| o.natural? and o.salvadorian? and o.el_salvador? and o.new_record?}
  validates :document_number, :document_type, presence: true, unless: Proc.new{|o| o.seen_document?}
  validates :nationality_name, presence: true, if: Proc.new{|o| o.foreign?}
  validates :terms, presence: true, if: Proc.new{|o| o.new_record?}
  # validates document_number format
  # validates :document_number, format: { with: /\d{8}-\d{1}$/, message: I18n.t('errors.messages.invalid_dui') }, if: :document_type_dui?
  validates :document_number, length: { minimum: 6 }, if: Proc.new{|o| o.passport?}
  validates :document_number, length: { minimum: 6 }, if: Proc.new{|o| o.resident_card? }
  validates :document_number, length: { minimum: 6 }, if: Proc.new{|o| o.student_card? }
  validates :document_number, length: { minimum: 6 }, if: Proc.new{|o| o.minority_card? }
  #validates_acceptance_of :terms

  validates_attachment :document_front_image,
  presence: true,
  content_type: { content_type: ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'] },
  size: { in: 0..10.megabytes },
  if: :require_attachment?

  validates_attachment :legal_representative_doc, presence: true, if: Proc.new{ |o| o.legal? }

  #validates :type_of_rights, acceptance: true, if: Proc.new{|o| o.new_record? && o.request_type == "personal"}

  #validate :at_least_one_type_of_rights, if: Proc.new{|o| o.request_type == "personal"}

  #def at_least_one_type_of_rights
  #  errors.add(:base, "Select atleast one output format type") if Proc.new{o.type_of_rights == nil}
  #end


  do_not_validate_attachment_file_type :legal_representative_doc
  do_not_validate_attachment_file_type :document_back_image
  do_not_validate_attachment_file_type :other_document

  # King Tokens
  #can_has_tokens :confirm_email, { days_valid: 5 }

  # Acts as TextCaptcha
  #acts_as_textcaptcha

  # Scope
  scope :confirmed, -> { where(:confirmed => 1 )}
  scope :from_website_exclude, -> {where.not("confirmed IS NULL AND from_website = ?", true)}
  #scope :expired, lambda { where('information_requests.delivery_deadline IS NOT NULL AND information_requests.delivery_deadline < ? AND state IN (?)', Date.current, [2,3]) } # change confirmed_at by admitted_at
  #scope :valid, -> { where(ignore_it: false) }

  scope :expired, -> (date) { where('information_requests.delivery_deadline IS NOT NULL AND information_requests.delivery_deadline < ? AND state IN (?)', date, [2,3]) }
  scope :by_tags,           -> (tag) { tagged_with("#{tag}", :any => true) }


  scope :are_not_closed, -> { where.not(state: [:closed, :not_corrected, :not_admitted])}
  scope :are_admitted, -> {where("admitted_at IS NOT NULL")}
  #scope :tag_counts_custom, -> (expedients_ids) { ActsAsTaggableOn::Tag.joins(:taggings).select('tags.id, tags.name, tags.taggings_count, COUNT(taggings.id) as count')
  #                            .group('tags.id, tags.name, tags.taggings_count').where(taggings: { taggable_type: 'Expedient', taggable_id: expedients_ids} )}

  ransacker :expired_test do
    expired
  end

  # delegates
  delegate :new?, :correct?, :not_corrected?, to: :current_state
  delegate :information_request_correlative, :acronym, to: :institution, allow_nil: true, prefix: true
  delegate :information_request_personal_correlative, :acronym, to: :institution, allow_nil: true, prefix: true

  # enums
  enum state: [:newer, :correct, :process, :extended, :closed, :not_corrected, :not_admitted]
  enum educational_level: [:unknown, :neither, :primary, :secondary, :highschool, :university, :postgrade]
  enum nationality: [:salvadorian, :foreign]
  enum residence: [:el_salvador, :other]
  enum entity_type: [:natural, :legal]
  enum gender: [:female, :male]
  enum reception: [:in_person, :email_reception, :website]
  enum document_type: [:dui, :passport, :resident_card, :student_card, :minority_card, :license_card, :lawyer_card, :another_one]
  enum delivery_way: [:cd, :dvd, :usb, :photocopy, :certified_photocopy, :email, :fax, :mail, :direct_consultation]
  enum extension_type: [:no_one, :antiquity, :complexity, :antiquity_and_complexity]
  enum request_type: [:normal, :personal]
  enum iaip_process_kind: [:no_process, :in_iaip]


  after_create :add_no_process_iaip_state

  def deadline_date(dynumber, datetime = Time.zone.now)
    datetime = deadline_date_start(datetime)

    loop do
      dynumber -= 1 if institution.work_date?(datetime)
      datetime += 1.days
      break if dynumber.zero?
    end
    datetime - 1.days
  end

  def business_days_passed
    datetime = admitted_at.to_date
    dynumber = 0
    loop do
      dynumber += 1 if institution.work_date?(datetime)
      datetime += 1.days
      break if datetime.end_of_day >= Date.today.end_of_day
    end
    dynumber
  end

  def deadline_date_start(datetime)
    schedule = institution.date_schedule_for(datetime)
    return datetime.to_date + 1.days if schedule.nil? ||
                                        !institution.business_time?(datetime)
    datetime.to_date
  end

  def correct_due_date
    last_event = self.events.where(state: "correct").last
    due_date = self.deadline_date(InformationRequest::CORRECT_BUSINESS_DAYS, last_event.event_start.beginning_of_day)
    #if self.requirements.where(meets_depth: true).any?
      #due_date = self.deadline_date(InformationRequest::REQUIREMENT_CORRECT_BUSINESS_DAYS, last_event.event_start)
    #else
      #due_date = self.deadline_date(InformationRequest::CORRECT_BUSINESS_DAYS, last_event.event_start)
    #end
    due_date
  end

  def remaining_days
    days = 0
    if self.correct?
      days = (self.correct_due_date - Date.current).to_i
    else
      days = (self.delivery_deadline - Date.current).to_i
    end
    if self.closed_request?
      days = '-'
    end
    days.to_s
  rescue
    '-'
  end

  def remaining_days_for_personal
    days = 0
    days = (self.delivery_deadline - Date.current).to_i
    if self.closed_request?
      days = '-'
    end
    days.to_s
  end

  def new_or_existing
    if self.iaip_process.nil?
      return "/panel/information_requests/#{self.id}/information_request_iaip_processes/new"
    else
      return "/panel/information_requests/#{self.id}/information_request_iaip_processes/#{self.iaip_process.id}"
    end
  end

  def boolean_new_or_existing
    self.iaip_process.nil? ? "" : "✔"
  end

  def percent
    if self.state == "process"
      total_days = (self.delivery_deadline.to_date - self.admitted_at.to_date).to_i
      passed_days = self.remaining_days.to_i
      progress = ((passed_days * 100) / total_days).ceil
      progress = 100 if progress > 100
      progress = -100 if progress < -100
      progress.to_i
    end
  rescue
    nil
  end

  def tags_string
    tags.pluck(:name).join(', ')
  end


  def self.ransackable_scopes(auth_object = nil)
    [:by_tags]
  end

  def update_delivery_deadline
    correct_date = self.events.where(state: "correct").first.created_at if self.events.map(&:state).include?("correct")
    corrected_date = self.events.where(state: "corrected").first.created_at if self.events.map(&:state).include?("corrected")

    if self.events.map(&:state).include?("corrected")
      # Fecha en que se pausa la solicitud (previene)
      date = events.where(state: "correct").first.created_at.to_time
    else
      date = admitted_at.try(:to_time)
    end

    datetime = admitted_at.to_time
    dynumber = 0

    loop do
      break if datetime.end_of_day >= date.end_of_day
      dynumber += 1 if institution.work_date?(datetime.to_date)
      datetime += 1.days
    end
    #if correct_date && corrected_date
    #  added_days = (corrected_date.to_date - correct_date.to_date).to_i
    #  dynumber = dynumber - added_days
    #end
    dynumber

    if date
      days = case extension_type.to_s
        when 'antiquity'
          20-dynumber
        when 'complexity'
          15-dynumber
        when 'antiquity_and_complexity'
          25-dynumber
        else
          10-dynumber
      end
      if correct_date && corrected_date
        start_date = corrected_date
      else
        start_date = date
      end

      update_column(:delivery_deadline, deadline_date(days, start_date)) if days
    end

  end

  def get_state_symbol
    InformationRequest.states.key(self.state)
  end

  def require_attachment?
    !email.nil? and !seen_document?
  end

  def generate_reference_code(length = 6)
    generated_code = (0..length).map{ rand(36).to_s(36) }.join
    return generated_code unless InformationRequest.exists?(reference_code: generated_code)

    generate_reference_code
  end

  def set_reference_code
    if self.reference_code.blank?
      self.reference_code = generate_reference_code.upcase
    end
  end

  def set_correlative
    if self.correlative.blank? and self.institution.present?
      self.correlative = "#{institution_acronym}-#{Date.current.year}-#{institution_information_request_correlative.to_s.rjust(4, '0')}"
      self.institution.next_correlative
    end
  end

  def add_no_process_iaip_state
    self.iaip_process_kind = :no_process
    self.save!(validate: false)
  end

  def deliver_confirmation_email_request
    rsa_private = OpenSSL::PKey::RSA.generate 512
    rsa_public = rsa_private.public_key
    exp = Time.now.to_i + 4 * 3600
    payload = {exp: exp, email: self.email}
    custom_token = JWT.encode payload, rsa_private, 'RS256'
    self.token = custom_token
    self.save!
    UserMailer.delay.information_request_email_confirm(self)
  end

  def deliver_confirmation_email_personal_request
    rsa_private = OpenSSL::PKey::RSA.generate 512
    rsa_public = rsa_private.public_key
    exp = Time.now.to_i + 4 * 3600
    payload = {exp: exp, email: self.email}
    custom_token = JWT.encode payload, rsa_private, 'RS256'
    self.token = custom_token
    self.save!
    UserMailer.delay.personal_information_request_email_confirm(self)
  end


  def deliver_information_request
    UserMailer.delay.new_information_request(self)
  end

  def deliver_personal_information_request
    UserMailer.delay.new_personal_information_request(self)
  end

  def deliver_information_request_user_notification
    UserMailer.new_information_request_user_notification(self).deliver if email.present?
  end

  def deliver_information_request_user_create
    UserMailer.new_information_request_user_create(self).deliver if email.present?
  end

  def document_image_extension(side = :front)
    File.extname(self.send("document_#{side.to_s}_image").path)
  end

  def readable_document_image_filename(side = :front)
    [ lastname,
      firstname,
      '-',
      self.document_type,
      '(' + I18n.t("activerecord.attributes.information_request.document_image.#{side.to_s}") + ')'
    ].join(' ') + self.document_image_extension(side)
  end

  def fullname
    [firstname, lastname].join(' ')
  end

  def birthday_year
    birthday.year
  end

  def get_info_request_state
    I18n.t("activerecord.enum.information_request.state.#{state}", default: status.titleize) rescue self.state
  end

  def get_state
    #@get_state ||= I18n.t("label.#{current_state}")
    @get_state ||= I18n.t("label.#{state}")
  end

  def self.new_requests
    where(state: ['newer'])
  end

  def self.process_requests
    #joins(:events).merge InformationRequestEvent.with_last_state(['process','extended'])
    where(state: ['process', 'extended'])
  end

  def self.correct_requests
    #joins(:events).merge InformationRequestEvent.with_last_state('correct')
    where(state: 'correct')
  end

  def self.closed_requests
    #joins(:events).merge InformationRequestEvent.with_last_state(['closed','not_corrected','not_admitted'])
    where(state: ['closed', 'not_corrected', 'not_admitted'])
  end

  def self.not_admitted_requests
    where(state: ['not_admitted'])
  end

  def self.expired_requests
    #expired.joins(:events).merge InformationRequestEvent.with_last_state(['process','extended'])
    where(state: ['process', 'extended']).where('information_requests.delivery_deadline < ?', Date.current)
  end

  def can_process?
    @can_process ||= (self.newer? && self.meets_shape == true && self.meets_all_requirements and self.is_admitted?) or self.correct? or (self.newer? && self.meets_shape == true && self.meets_all_requirements and self.is_admitted? && !self.can_correct?)
  end

  def can_correct?
    # can correct if status is new or if process status is in 3 business days range, and there is not a previous correction
    @can_correct ||= self.has_not_been_corrected? and !self.closed? and self.is_admitted? and self.requirements.where("meets_depth IS NOT NULL AND art_74_laip IS NOT NULL") and (self.deadline_date(4, self.deadline_date_start(self.admitted_at).to_time) - Date.today).to_i >= 0 and (self.requirements.where(meets_depth: false).size > 0 or self.meets_shape == false)

    # PROCESS ((self.correct_due_date - Date.today).to_i >= 0) and
    # Falta metodo de la fecha
    #@can_correct ||= (start_date_with_business(3) - Date.current).to_i >= 0 and self.has_not_been_corrected? and !closed? and is_admitted? rescue false
  end

  def expired_correct? #Prevencion vencida
    self.correct? && (self.deadline_date(4, self.deadline_date_start(self.admitted_at).to_time) - Date.today).to_i <= 0
  end

  def check_confirmed_date
    if self.admitted_at.to_date > Date.today + 1
      self.update_column(:confirmed_at, Time.now)
      self.update_column(:admitted_at, Time.now)
    end
    self.update_delivery_deadline
  end

  def meets_all_requirements
    self.requirements.where("meets_depth = ? OR art_74_laip = ?", true, true).count == self.requirements.count
  end

  def can_closed?
    @can_closed ||= ( !['new', 'correct'].include?(state) and has_requirements? and opened_requirements == 0 )
  end

  def has_not_been_corrected?
    @has_not_been_corrected ||= !events.pluck(:state).include?('correct')
  end

  def can_extended?
    @can_extended ||= ((extension_type != 'antiquity_and_complexity') && process? && self.request_type == "normal") or (self.request_type == "personal" && (!self.type_of_rights.blank? && self.type_of_rights.include?("access")) && self.extension_type == nil && (extension_type != 'antiquity_and_complexity') && process?)
  end

  def request_is_closed?
    @request_closed ||= self.closed? or self.not_corrected? or self.not_admitted?
  end

  #def percent
  #  @percent ||= self.process? ? (events.last.try(:percent, delivery_deadline) rescue nil) : nil
  #end

  # verify has requirements
  def has_requirements?
    @has_requirements ||= self.requirements.any?
  end

  # count of new requirements
  def opened_requirements
    #@opened_requirements ||= self.requirements.map(&:current_state).count('process')
  end

  def all_requirements_are_closed?
    @all_requirements_are_closed ||= self.requirements.map(&:state).all?{|x| x == "closed"}
  end

  def get_information_type
    InformationRequest::INFORMATION_TYPE[information_type]
  end

  def get_resolution_type
    s = ""
    InformationRequest::RESOLUTION_TYPE[get_information_type].each{|a| s = a[0] if a[1] == resolution_type }
    s
  end

  def closed_request?
    @closed_request ||= ['closed', 'not_corrected', 'not_admitted'].include?(state)
  end

  def current_state
    #@current_state ||= (events.order(:id).last.try(:state) || STATES.first).inquiry
    state.inquiry
  end

  def get_state
    #@get_state ||= I18n.t("label.#{current_state}")
    @get_state ||= I18n.t("label.#{state}")
  end

  def start_date
    @start_date ||= correct? ? Time.current : admitted_at
  end

  def notification_mode_string
    return '' unless notification_mode.present?
    Array.wrap(notification_mode).map{|n| NOTIFICATION_MODE[n]}.join(', ')
  end

  def type_of_rights_string
    return '' unless type_of_rights.present?
    Array.wrap(type_of_rights).map{|n| TYPES_OF_RIGHTS[n]}.join(', ')
  end

  def is_admitted?
    admitted_at.present?
  end

  def processed_at
    self.events.where(state: 'process').order('id DESC').limit(1).first.created_at rescue nil
  end

  def corrected_at
    self.events.where(state: 'correct').order('id DESC').limit(1).first.event_start rescue nil
  end

  def closed_at
    self.events.where(state: 'closed').order('id DESC').limit(1).first.created_at.to_date rescue nil
  end

  def process_days
    @status = self.events.order(:id)
    process_date = @status.select{|s| s.state == 'process'}.last.event_start.to_date rescue nil
    closed_date = @status.select{|s| s.state == 'closed'}.last.created_at.to_date rescue nil
    return nil if process_date.nil? or closed_date.nil?

    datetime = process_date
    dynumber = 0
    loop do
      dynumber += 1 if institution.work_date?(datetime)
      datetime += 1.days
      break if datetime.end_of_day >= closed_date
    end
    dynumber

  end



=begin
  def start_date_with_business(days, custom_date = nil)
    business_date = custom_date ? custom_date : ( correct? ? Date.current : ( admitted_at.try(:to_date) || Date.current ) )
    # obtain all business days
    business_days = CalendarEvent.select('kind,event_date').joins(:institution_calendar).where('institution_calendars.institution_id = ? AND calendar_events.event_date > ?', institution_id, business_date.beginning_of_day).group_by{|o| o.event_date.to_date}
    while days > 1 # 0 first business day is current day
      business_date = business_date + 1.day
      if business_days.has_key?(business_date)
        # rest a day unless is a holiday
        days -= 1 unless business_days[business_date].map(&:kind).include?(:holiday)
      else
        # rest a day unless is weekened
        days -= 1 unless business_date.saturday? or business_date.sunday?
      end
    end
    business_date
  end

  def add_complexity_days(days = 5)
    while days > 1 # 0 first business day is current day
      self.delivery_deadline = self.delivery_deadline + 1.day
      days -= 1 unless delivery_deadline.saturday? or delivery_deadline.sunday?
    end
  end



  def process_days
    @status = events.order(:id)
    return nil unless @status.size > 1
    # obtain process and closed date
    process_date = @status.select{|s| s.state == 'process'}.last.try(:event_start)
    closed_date = @status.select{|s| s.state == 'closed'}.last.try(:created_at)
    return nil if process_date.nil? or closed_date.nil?
    # obtain all business days between event dates
    business_days = CalendarEvent.select('kind,event_date').joins(:institution_calendar).where('institution_calendars.institution_id = ? AND calendar_events.event_date BETWEEN ? AND ?', institution_id, process_date.beginning_of_day, closed_date.end_of_day).group_by{|o| o.event_date.to_date}
    cursor_day = process_date
    counted_days = 0
    while cursor_day <= closed_date.to_date
      if business_days.has_key?(cursor_day)
        # rest a day unless is a holiday
        counted_days += 1 unless business_days[cursor_day].map(&:kind).include?(:holiday)
      else
        # rest a day unless is weekened
        counted_days += 1 unless cursor_day.saturday? or cursor_day.sunday?
      end
      cursor_day = cursor_day + 1.day
    end
    counted_days
  end

  def set_event_date(date = nil)
    return unless date
    begin
      nd = Date.parse(date)
      # update admitted at
      update_column(:admitted_at, Time.zone.local(nd.year, nd.month, nd.day, admitted_at.hour, admitted_at.min, admitted_at.sec))
      # update delivery deadline
      update_column(:delivery_deadline, start_date_with_business(10))
      # update process date
      e = events.select{|o| o.state == 'process'}.first
      if e
        e.update_column(:event_start, nd)
        e.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, e.created_at.hour, e.created_at.min, e.created_at.sec))
        e.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, e.updated_at.hour, e.updated_at.min, e.updated_at.sec))
      end
      # update requirements start
      comments.requirements.each do |r|
        r.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, r.created_at.hour, r.created_at.min, r.created_at.sec))
        r.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, r.updated_at.hour, r.updated_at.min, r.updated_at.sec))
        e = r.events.select{|o| o.state == 'process'}.first
        if e
          e.update_column(:event_start, nd)
          e.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, e.created_at.hour, e.created_at.min, e.created_at.sec))
          e.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, e.updated_at.hour, e.updated_at.min, e.updated_at.sec))
        end
      end
    end
  end

  def set_end_at(date = nil)
    return unless date
    begin
      nd = Date.parse(date)
      # update closed date
      e = events.select{|o| o.state == 'closed'}.first
      if e
        e.update_column(:event_start, nd)
        e.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, e.created_at.hour, e.created_at.min, e.created_at.sec))
        e.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, e.updated_at.hour, e.updated_at.min, e.updated_at.sec))
      end
      # update requirements end
      comments.requirements.each do |r|
        e = r.events.select{|o| o.state == 'closed'}.first
        if e
          e.update_column(:event_start, nd)
          e.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, e.created_at.hour, e.created_at.min, e.created_at.sec))
          e.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, e.updated_at.hour, e.updated_at.min, e.updated_at.sec))
        end
        # update individual requirements created and updated
        r.information_request_requirements.each do |r2|
          r2.update_column(:created_at, Time.zone.local(nd.year, nd.month, nd.day, r2.created_at.hour, r2.created_at.min, r2.created_at.sec))
          r2.update_column(:updated_at, Time.zone.local(nd.year, nd.month, nd.day, r2.updated_at.hour, r2.updated_at.min, r2.updated_at.sec))
        end
      end
    end
  end

=end
end
