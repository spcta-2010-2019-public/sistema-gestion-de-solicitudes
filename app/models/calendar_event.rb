#
class CalendarEvent < ApplicationRecord
  belongs_to :institution_calendar

  validates :institution_calendar_id, :kind, :name, :event_date, presence: true

  # before_validation :ensure_kind

  enum kind: [:holiday, :work]

  # def ensure_kind
  #   self.kind = 'holiday' unless kind.present?
  # end

  scope :holiday_dates, -> { where(kind: CalendarEvent.kinds[:holiday]) }
  scope :work_dates,    -> { where(kind: CalendarEvent.kinds[:work])    }

  after_save :update_requests_deadline

  def update_requests_deadline
    requests = InformationRequest.where(institution_id: self.institution_calendar.institution_id).are_admitted.are_not_closed
    if requests.size > 0
      requests.each do |request|
        request.update_delivery_deadline
      end
    end
  end


end
