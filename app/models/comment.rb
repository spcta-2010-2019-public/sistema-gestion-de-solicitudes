class Comment < ApplicationRecord

  belongs_to :requirement
  belongs_to :user

  validates :message, presence: true

  has_attached_file :document

  do_not_validate_attachment_file_type :document

end
