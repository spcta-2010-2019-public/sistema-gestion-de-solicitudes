class Api::DropdownsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:state_cities]

  def state_cities
    @state = State.find(params[:id])
    @cities = {}
    @state.cities.each do |c|
      @cities[c.id] = c.name
    end
    respond_to do |format|
      format.json { render json: @cities }
    end
  end
end
