class Api::InformationRequestsController < ApplicationController
  protect_from_forgery except: :create

  def create
    @information_request = InformationRequest.new
    set_attributes @information_request, item_params
    @information_request.notification_mode = params[:notification_mode] || []
    @information_request.requirements.build unless @information_request.requirements.any?
    @information_request.birthday = Date.new(params[:information_request]["birthday(1i)"].to_i,
                         params[:information_request]["birthday(2i)"].to_i,
                         params[:information_request]["birthday(3i)"].to_i)
    @information_request.from_website = true
    @information_request.state = "newer"
    if @information_request.save

      @information_request.deliver_confirmation_email_request
      #session[:information_request_id] = @information_request.id
      #redirect_to please_confirm_www_information_requests_path
      respond_to do |format|
        result         = {}
        result[:email] = @information_request.email
        format.json { render :json => { :success => result }, :status => 200 }
      end

    else
      # @information_request.errors.each do |i, val|
      #   puts "===> #{i.inspect} #{val.inspect}"
      # end
      respond_to do |format|
        format.json { render :json => { :error => @information_request.errors }, :status => 422 }
      end
    end
  end

  def controls
    @controls = {}
    @controls[:institutions] = {}
    Institution.all.each do |i|
      @controls[:institutions][i.id] = i.name
    end

    @controls[:nationality_radios] = {}
    InformationRequest.nationalities.each do |n|
      @controls[:nationality_radios]["#{n[1]}"] = [n[0], t("activerecord.enum.information_request.nationality.#{n[0]}"), (n[0] == 'salvadorian' ? 'checked' : '')]
    end

    @controls[:nationality_dropdown] = {}
    Nationality.all.each do |n|
      @controls[:nationality_dropdown][n.id] = n.name
    end

    @controls[:residences] = {}
    InformationRequest.residences.each do |n|
      @controls[:residences]["#{n[1]}"] = [n[0], t("activerecord.enum.information_request.residence.#{n[0]}"), (n[0] == 'el_salvador' ? 'checked' : '')]
    end

    @controls[:entity_types] = {}
    InformationRequest.entity_types.each do |e|
      @controls[:entity_types]["#{e[1]}"] = [e[0], t("activerecord.enum.information_request.entity_type.#{e[0]}"), (e[0] == 'natural' ? 'checked' : '')]
    end

    @controls[:genders] = {}
    InformationRequest.genders.each do |g|
      @controls[:genders]["#{g[1]}"] = [g[0], t("activerecord.enum.information_request.gender.#{g[0]}"), (g[0] == 'female' ? 'checked' : '')]
    end

    @controls[:educational_levels] = {}
    InformationRequest.educational_levels.each do |e|
      @controls[:educational_levels]["#{e[1]}"] = [e[0], t("activerecord.enum.information_request.educational_level.#{e[0]}")]
    end

    @controls[:occupations] = {}
    Occupation.reorder("name asc").each do |o|
      @controls[:occupations][o.name] = o.id
    end

    @controls[:states] = {}
    State.all.each do |s|
      @controls[:states][s.name] = s.id
    end

    @controls[:notification_modes] = {}
    InformationRequest::NOTIFICATION_MODE.each do |n|
      @controls[:notification_modes]["#{n[0]}"] = n[1]
    end

    @controls[:delivery_ways] = {}
    InformationRequest.delivery_ways.each do |d|
      @controls[:delivery_ways]["#{d[0]}"] = t("activerecord.enum.information_request.delivery_way.#{d[0]}")
    end

    @controls[:document_types] = {}
    InformationRequest.document_types.each do |d|
      @controls[:document_types]["#{d[0]}"] = t("activerecord.enum.information_request.document_type.#{d[0]}")
    end

    respond_to do |format|
      format.html do
        redirect_to 'https://gobiernoabierto.gob.sv/information_requests/new'
      end
      format.json { render json: @controls }
    end
  end

  private
    def set_attributes (item, params)
      item.attributes=params
    end

    def item_params
      params.require(:information_request).permit(
          :address,
          :admitted_at,
          :birthday,
          :business_days,
          :cellular,
          :city_id,
          :confirmed,
          :delivery_deadline,
          :delivery_way,
          :document_back_image,
          :document_front_image,
          :document_number,
          :document_type,
          :educational_level,
          :email_confirmation,
          :email,
          :entity_type,
          :estimated_business_days,
          :extension_justification,
          :extension_type,
          :fax,
          :firstname,
          :gender,
          :information_type,
          :institution_id,
          :lastname,
          :legal_representative_doc,
          :legal_representative,
          :nationality_name,
          :nationality,
          :notification_mode,
          :occupation_id,
          :phone,
          :reference_code,
          :requested_information,
          :residence,
          :resolution_date,
          :resolution_type,
          :resource,
          :seen_document,
          :state_id,
          :terms,
          requirements_attributes: [:requested_information]
      )
    end
end
