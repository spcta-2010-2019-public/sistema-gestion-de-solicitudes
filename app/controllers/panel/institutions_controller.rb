# frozen_string_literal: true
module Panel
  #
  class InstitutionsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      Institution
    end

    def respond_to_html
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(:many), index_url
      render template: 'panel/institutions/index'
    end

    def new
      @item = model.new

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/panel/form'
    end

    def create
      @item = model.new item_params
      set_uniq_institution!
      return redirect_to(edit_url, flash: { saved: true }) if @item.save

      init_form
      breadcrumbs_for(:new)
      render template: 'concerns/panel/form'
    end

    def edit
      init_form
      breadcrumbs_for(:edit)

      render template: 'concerns/panel/form'
    end

    def update
      respond_to do |format|
        format.json { super }
        format.html do
          return redirect_to(edit_url, flash: { saved: true }) if
            @item.update_attributes(item_params)

          breadcrumbs_for(:edit)
          init_form
          render template: 'concerns/panel/form'
        end
      end
    end

    def respond_to_json
      if current_user.admin?
        @q = model
      elsif current_user.oir?
        @q = model.is_enabled.where(id: current_user.institution_ids)
      end
      @q = @q.ransack(params[:filter])
      @c = @q.result.paginate(page: params[:page], per_page: params[:count])
      render json: json_result(@c)
    end

    def item_params
      perms = params.require(:item).permit(permits)
      #perms.delete(:institution_id) if current_user.one_institution?
      perms
    end

    def permits
      [:information_request_correlative, :officer_name, :officer_email,
        :enabled, :information_request_personal_correlative]
    end

  end
end
