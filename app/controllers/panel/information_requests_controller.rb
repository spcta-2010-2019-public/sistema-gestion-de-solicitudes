# frozen_string_literal: true
module Panel
  #
  class InformationRequestsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    load_and_authorize_resource
    before_action :set_item, only: [:show, :edit, :update, :destroy, :edit_personal, :update_personal]
    before_action :set_uniq_institution!, only: [:update, :update_personal]
    before_action :check_schedules, only: [:show, :admitted]

    def check_schedules
      @item = InformationRequest.find(params[:id])
      if (@item.state != "closed" || @item.state != "not_admitted") && @item.institution.institution_schedules.blank?
        redirect_to panel_information_requests_url, flash: {no_schedules: true}
      end
    end


    def model
      InformationRequest
    end

    def permits
      [:name, :institution_id, :nationality, :nationality_name, :firstname,
      :lastname, :email, :document_type, :document_number, :entity_type,
      :document_front_image, :document_back_image, :other_document, :gender,
      :birthday, :authorized_person, :phone, :cellular, :fax, :educational_level,
      :residence, :address, :state_id, :city_id, :occupation_id, :delivery_way,
      :notification_mode, :reference_code, :confirmed, :confirmed_at,
      :admitted_at, :delivery_deadline, :extension_type, :resolution_type,
      :correlative, :redirected_institution_id, :seen_document, :ignore_it,
      :type_of_rights, :requested_personal_info, :request_type,
      :from_website, :resource, :state, :meets_shape, :legal_representative,
      :legal_representative_doc, :reception, tag_list: [],
      requirements_attributes: [:id, :requested_information, :_destroy],
      events_attributes: [:id, :justification, :event_start, :document, :user_id,
      :state]]
    end

    def event_permits
      [:user_id, :justification, :document, :event_start]
    end

    def exportable_fields
      [:name]
    end

    def new
      @item = model.new
      2.times do
        @item.requirements.build
      end
      init_form
      breadcrumbs_for(:new)
      render template: 'concerns/panel/form'
    end

    def new_personal
      @item = model.new
      2.times do
        @item.requirements.build
      end
      init_form
      breadcrumbs_for(:new_personal)
    end

    def create_personal
      @item = model.new item_params
      @item.notification_mode = params[:notification_mode] || []

      @item.type_of_rights = params[:type_of_rights] || ["access"] #Si no hay ninguno chequeado

      @item.from_website = false
      @item.confirmed = true
      @item.confirmed_at = Time.current
      @item.terms = true

      if @item.save
        @item.state = "newer"
        #@item.set_reference_code
        #@item.set_correlative

        #@item.delivery_deadline = @information_request.start_date_with_business(10)
        @item.save

        return redirect_to(index_url, flash: { saved: true })
      else
        init_form
        breadcrumbs_for(:new_personal)
        render template: 'concerns/panel/form'
      end

    end

    def edit_personal
      init_form
      breadcrumbs_for(:edit_personal)
    end

    def update_personal
      @item.assign_attributes(item_params)
      @item[:notification_mode] = params[:notification_mode] || []
      @item[:type_of_rights] = params[:type_of_rights] || []
      respond_to do |format|
        format.json { super }
        format.html do
          return redirect_to(show_url, flash: { saved: true }) if
            @item.save

          breadcrumbs_for(:edit_personal)
          init_form
        end
      end
    end

    def update
      @item.assign_attributes(item_params)
      @item.notification_mode = params[:notification_mode] || []

      respond_to do |format|
        format.json { super }
        format.html do
          return redirect_to(edit_url, flash: { saved: true }) if
            @item.save

          breadcrumbs_for(:edit)
          init_form
          render template: 'concerns/panel/form'
        end
      end
    end

    def closure
      @item = model.find(params[:id])
      @item.events.build
      breadcrumbs_for(:closure, @item)
      redirect_to(show_url(@item)) if @item.all_requirements_are_closed? == false
    end

    def create
      @item = model.new item_params
      @item.requirements.build unless @item.requirements.any?
      @item.notification_mode = params[:notification_mode] || []
      @item.from_website = false
      @item.confirmed = true
      @item.confirmed_at = Time.current
      @item.terms = true

      if @item.save
        @item.state = "newer"
        #@item.set_reference_code
        #@item.set_correlative

        #@item.delivery_deadline = @information_request.start_date_with_business(10)
        @item.save

        return redirect_to(index_url, flash: { saved: true })
      else
        init_form
        breadcrumbs_for(:new)
        render template: 'concerns/panel/form'
      end
    end

    def index
      if current_user.admin?
        @information_requests = model
      elsif current_user.oir?
        @information_requests = model.confirmed.from_website_exclude.where("institution_id IN (?)", current_user.institution_ids)
      else
        @information_requests = model.joins(:requirements).where("requirements.assigned_users @> ? OR requirements.invited_users @> ?", "{#{current_user.id}}", "{#{current_user.id}}").uniq
      end

      respond_to do |format|
        format.html { super }
        format.json { respond_to_json }
      end
    end

    def respond_to_html
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(:many), index_url
      render template: 'panel/information_requests/index'
    end

    def respond_to_json
      if current_user.admin?
        @q = model.reorder("information_requests.admitted_at DESC, information_requests.correlative DESC, information_requests.updated_at DESC")
      elsif current_user.oir?
        @q = model.confirmed.from_website_exclude.where("institution_id IN (?)", current_user.institution_ids).reorder("information_requests.admitted_at DESC, information_requests.correlative DESC, information_requests.updated_at DESC")
      else
        @q = model.joins(:requirements).where("requirements.assigned_users @> ? OR requirements.invited_users @> ?", "{#{current_user.id}}", "{#{current_user.id}}").uniq
      end
      case params[:status].to_s
        when 'new'
          @q = @q.new_requests
        when 'corrected'
          @q = @q.correct_requests
        when 'process'
          @q = @q.process_requests
        when 'closed'
          @q = @q.closed_requests
        when 'expired'
          @q = @q.expired_requests
        else
          @q
      end
      if params[:filter] && params[:filter][:state_eq] && params[:filter][:state_eq].include?("4")
        params[:filter][:state_eq] = []
        params[:filter][:state_in] = [4,5,6]
      end
      if params[:filter] && params[:filter][:state_eq] && params[:filter][:state_eq].include?("7")
        params[:filter][:state_in] = []
        params[:filter][:state_eq] = []
        @q = @q.expired_requests
        @q = @q.ransack(params[:filter])
      else
        @q = @q.ransack(params[:filter])
      end

      @c = @q.result.paginate(page: params[:page], per_page: params[:count])

      render json: json_result(@c)
    end

    def show
      @item = InformationRequest.find(params[:id]).decorate
      breadcrumbs_for(:show)
      @last_event ||= @item.events.last
      @item.events.build

      @not_admitted_select = [["Duplicada", "duplicated"], ["Consulta/Orientación (Atención OIR)", "consultation"]]
      @tags = ActsAsTaggableOn::Tag.for_context(:tags)

      if current_user.administrative_unit?
        redirect_to(index_url) if @item.requirements.where("requirements.assigned_users @> ? OR requirements.invited_users @> ?", "{#{current_user.id}}", "{#{current_user.id}}").blank?
      end
    end

    def download_documents
      require 'rubygems'
      require 'zip'
      @item = InformationRequest.find(params[:id]).decorate
      #fr_documents = @item.requirements.map(&:file).sort_by!{ |o| o.created_at.to_i }.flatten.compact
      fr_documents = @item.requirements
      #comment_documents = Comment.where(requirement_id: [@item.requirements.map(&:id)]).map(&:document).sort_by!{ |o| o.created_at.to_i }.flatten.compact
      comment_documents = Comment.where(requirement_id: [@item.requirements.map(&:id)])
      #.delete_if{ |k, v| v.blank? }

      #Attachment name
      file_name = "Documentos-#{information_request.correlative}.zip"
      temp_file = Tempfile.new(file_name)

      begin
        #This is the tricky part
        #Initialize the temp file as a zip file
        Zip::OutputStream.open(temp_file) { |zos| }

        #Add files to the zip file as usual
        Zip::File.open(temp_file.path, Zip::File::CREATE) do |zipfile|
          fr_documents.each_with_index do |doc, i|
            unless doc.file.blank?
              zipfile.add("#{i + 1}.#{doc.file_file_name}", doc.file.path)
            end
          end
          comment_documents.each_with_index do |doc, i|
            unless doc.document.blank?
              zipfile.add("#{i + 1}.#{doc.document_file_name}", doc.document.path)
            end
          end
        end

        #Read the binary data from the file
        zip_data = File.read(temp_file.path)

        #Send the data to the browser as an attachment
        #We do not send the file directly because it will
        #get deleted before rails actually starts sending it
        send_data(zip_data, :type => 'application/zip', :disposition => 'attachment', :filename => file_name)
      ensure
        #Close and delete the temp file
        temp_file.close
        temp_file.unlink
      end
    end

    def download_request
      @item = InformationRequest.find(params[:id]).decorate
      breadcrumbs_for(:show)
      @last_event ||= @item.events.last
      @item.events.build
      @not_admitted_select = [["Incompetente", "incompetent"], ["Improcedente", "unreasonable"]]
      respond_to do |format|
        format.html
        format.pdf do
          render :pdf => "solicitud_#{@item.correlative}", :layout => "layouts/panel/pdf.html"
        end
      end
    end

    def get_states
      render json: @states = InformationRequest.states.to_json
    end

    def process_request
      @item = InformationRequest.find(params[:id])
      if @item.update_attributes item_params
        #CORREO
        @event ||= @item.events.last
        if @event && @event.state == "extended"
          @item.update_delivery_deadline
          return redirect_to(show_url, flash: { extended: true })
        elsif @event && @event.state == "corrected"
          if @item.request_type == "normal"
            @item.update_delivery_deadline
          #elsif @item.request_type == "personal"
          end
          return redirect_to(show_url, flash: { corrected: true })

        elsif @event && @event.state == "not_corrected"
          @item.requirements.each do |requirement|
            requirement.state = "closed"
            event = requirement.events.new
            event.event_start = Time.now
            event.user_id = current_user.id
            if @event.state == "not_corrected"
              if params[:information_type] == "unreasonable"
                requirement.information_type = "unreasonable"
                requirement.resolution_type = params[:resolution_type]
                event.justification = "Se le da cierre al requerimiento por Articulo 74-#{params[:resolution_type] == 'art_74_a_laip' ? 'A': 'C'} de la LAIP"
              else
                requirement.information_type = "incompetent"
                requirement.resolution_type = "art_68_laip"
                event.justification = "Se le da cierre al requerimiento por Articulo 68 de la LAIP"
              end

            else
              requirement.information_type = "dismissed"
              requirement.resolution_type = "not_corrected"
              event.justification = "Se le da cierre al requerimiento por solicitud no subsanada"
            end
            requirement.save!
            event.save!
          end
          return redirect_to(show_url, flash: { @item.state.to_sym => true })
        elsif @event && @event.state == "not_admitted"
          @item.set_correlative
          @item.save
          return redirect_to(show_url, flash: { @item.state.to_sym => true })
        else
          return redirect_to(show_url, flash: { @item.state.to_sym => true })
        end
      else
        return redirect_to(show_url, flash: { something_went_wrong: true })
      end
    end

    def admitted
      @item = InformationRequest.find(params[:id])
      @item.set_reference_code
      @item.set_correlative
      @item.admitted_at = Time.current
      @item.delivery_deadline = @item.deadline_date(10, @item.deadline_date_start(@item.admitted_at.to_time).to_time)
      @item.save(validate: false)
      #@item.deliver_information_request_user_create
      @item.delay.deliver_information_request_user_notification
      redirect_to panel_information_request_url(@item.id)
    end

    def admit_date
      @item = InformationRequest.find(params[:id])
      @item.update_attributes(item_params)
      @item.check_confirmed_date
      redirect_to panel_information_request_url(@item.id), flash: {date_corrected: true}
    end

    def tags
      information_request = InformationRequest.find(params[:id])
      begin
        information_request.tag_list = params[:information_request][:tag_list]
        information_request.save
      end
    end

    def statistics
      prepare_data
      breadcrumbs_for(:statistics)
    end

    def basic_csv_export
      require 'csv'
      prepare_data
      csv_string = CSV.generate do |csv|
        csv << ["Estadísticas SGS #{Date.current.try(:strftime, '%d-%m-%Y')}"]
        csv << ['Solicitudes por tipo de entrada']
        csv << ['Desde la web', @website_requests]
        csv << ['Manuales', @manual_requests]
        csv << ['Total', @total_requests]
        csv << ['Solicitudes por estado']
        csv << ['Nuevas', @new_requests]
        csv << ['En proceso', @process_requests]
        csv << ['Subsanadas', @correct_requests]
        csv << ['Cerradas', @closed_requests]
        csv << ['Vencidas', @expired_requests]
        csv << ['Total', @total_requests]
        csv << ['Gestión de requerimientos']
        csv << ['En proceso', @process_requirements]
        csv << ['Cerrados', @closed_requirements]
        csv << ['Total', @total_requirements]
        csv << ['Tipo de información entregada']
        csv << ['Pública', @public]
        csv << ['Reservada', @reserved]
        csv << ['Confidencial', @confidential]
        csv << ['Inexistente', @non_existent]
        csv << ['Incompetente', @incompetent]
        csv << ['Desestimada', @undelivered]
        csv << ['No se dio trámite', @not_processed]
        csv << ['Total', @total_information]
      end
      send_data csv_string, type: 'application/excel', filename: "Estadisticas-#{Date.current.try(:strftime, '%Y%m%d')}.csv", disposition: 'attachment'
    end

    def advanced_csv_export
      #prepare_advanced_data
      require 'csv'
      # prepare data
      view = params['view'] || 'detailed'
      ### VISTA DETALLADA
      if view.to_s == 'detailed'
        ### PREPARE CSV HEADER
        csv_string = CSV.generate do |csv|
          csv << ["Estadísticas avanzadas SGS #{Date.current.try(:strftime, '%d-%m-%Y')} - Detalle"]
          csv << [
                    'Institución',
                    'Total solicitudes recibidas',
                    'Total de solicitudes cerradas',
                    'Total de solicitudes sin trámite',
                    'Total de solicitudes en trámite',
                    'Total de requerimientos recibidos',
                    'Requerimientos oficiosos',
                    'Requerimientos públicos',
                    'Requerimientos no entregados por ser confidenciales',
                    'Requerimientos de datos personales',
                    'Requerimientos no entregados por ser reservados',
                    'Requerimientos versiones públicas de información reservada y confidencial',
                    'Requerimientos inexistentes',
                    'Requerimientos desestimados',
                    'Requerimientos re-direccionados',
                    'Requerimientos sin trámite',
                    ###
                    'Requerimientos en proceso',
                    'Promedio de atención de las solicitudes', # Tiempo en que se cerraron las solicitudes en el rango de fechas seleccionado
                    'Días que menos se tardaron', # Del anterior sacar el menor valor
                    'Días que mas se tardaron', # Del anterior sacar el mayor
                    'Total de solicitudes atendidas por antigüedad', # Todos los que hicieron extensiones este mes
                    'Total de solicitudes atendidas por complejidad', # Todos los que hicieron extensiones este mes
                    'Total de solicitudes atendidas por antigüedad y complejidad', # Todos los que hicieron extensiones este mes
                    'Solicitudes hechas por mujeres',
                    'Solicitudes hechas por hombres'
                ]
          ### PREPARE GLOBAL VARS
          requests = system_requests = system_closed_requests = system_process_requests = system_no_process_requests = system_process_requirements = history_records = []
          institutions = params[:institution_id].present? ? Institution.order(:name).where(id: params[:institution_id]) : Institution.order(:name)
          ### PREPARE DATA
          if params[:from_system].present?
            #system_requests = InformationRequest.confirmed.valid.accessible_by(current_ability)
            if current_user.has_role?("admin")
              system_requests = model.confirmed
            else
              system_requests = model.where("institution_id IN (?)", current_user.institution_ids)
            end


            system_requests = system_requests.where('information_requests.admitted_at > ?', Date.parse(params[:system_start_at]).beginning_of_day) if params[:system_start_at].present?
            system_requests = system_requests.where('information_requests.admitted_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
            system_requests = system_requests.where(institution_id: params[:institution_id]) if params[:institution_id].present?

            # all closed requests
            #selected_ids = ActiveRecord::Base.connection.execute("SELECT MAX(id) FROM information_request_events WHERE eventable_type = 'InformationRequest' AND information_request_events.created_at > '#{params[:system_start_at].present? ? Date.parse(params[:system_start_at]).beginning_of_day : (Date.current - 5.year).beginning_of_day}' AND information_request_events.created_at < '#{params[:system_end_at].present? ? Date.parse(params[:system_end_at]).end_of_day : Date.current.end_of_day}' GROUP BY eventable_id").to_a.flatten
            system_closed_requests = requests.closed_requests
            # all process requests
            system_process_requests = requests.process_requests
            # all no process requests
            system_no_process_requests = requests - system_process_requests - system_closed_requests
            system_no_process_requests = system_no_process_requests.where('information_requests.confirmed_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?

            system_process_requirements = Requirement.where("information_request_id IN (?)", system_process_requests.map(&:id)).where("requirements.state = ?", "process")


            # all no process requests
            #system_no_process_requests = InformationRequest.confirmed.valid.where("information_requests.id NOT IN ( SELECT DISTINCT(eventable_id) FROM information_request_events WHERE eventable_type='InformationRequest')")
            #system_no_process_requests = system_no_process_requests.where('information_requests.confirmed_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
            # all process requirements
            #selected_ids = ActiveRecord::Base.connection.execute("SELECT MAX(id) FROM information_request_events WHERE eventable_type = 'Comment' AND information_request_events.created_at > '#{params[:system_start_at].present? ? Date.parse(params[:system_start_at]).beginning_of_day : (Date.current - 5.year).beginning_of_day}' AND information_request_events.created_at < '#{params[:system_end_at].present? ? Date.parse(params[:system_end_at]).end_of_day : Date.current.end_of_day}' GROUP BY eventable_id").to_a.flatten
            #system_process_requirements = Comment.requirements.joins(:events).where("comments.commentable_id IN (?) AND information_request_events.id IN (?) AND information_request_events.state = 'process'", system_requests.map(&:id), selected_ids)
          end
          if params[:from_history].present?
            history_records = InformationRequestRecord.order(:opening_date)
            history_records = history_records.where('opening_date >= :date OR (opening_date < :date AND closing_date >= :date)', date: Date.parse(params[:history_start_at])) if params[:history_start_at].present?
            history_records = history_records.where('closing_date <= :date OR (closing_date > :date AND opening_date <= :date)', date: Date.parse(params[:history_end_at])) if params[:history_end_at].present?
            history_records = history_records.where(institution_id: params[:institution_id]) if params[:institution_id].present?
          end
          ### EXPORT ROWS
          institutions.each do |institution|
            # system requests
            requests = system_requests.select{|o| o.institution_id == institution.try(:id)}
            # histories records by institution
            records = history_records.select{|o| o.institution_id == institution.try(:id)}

            unless requests.blank? and records.blank?
              # requirements in the period
              #managements = Comment.requirements.where(commentable_type: 'InformationRequest', commentable_id: requests.map(&:id))
              requirements = Requirement.where("information_request_id IN (?)", system_process_requests.map(&:id))

              # closed requests by institutions
              closed_requests = system_closed_requests.select{|o| o.institution_id == institution.try(:id)}
              # no process requests by institutions
              no_process_requests = system_no_process_requests.select{|o| o.institution_id == institution.try(:id)}
              # process requests by institutions
              process_requests = system_process_requests.select{|o| o.institution_id == institution.try(:id)}
              # process requirements by institutions
              process_requirements = system_process_requirements.select{|o| o.request.institution_id == institution.try(:id)}.size
              # average response
              average_response_requests = requests.map(&:process_days).compact
              csv << [
                # institución
                institution.nil? ? 'No se donde enviarla' : institution.name,
                # total de solicitudes
                requests.size + (records.map(&:total_requests).compact.inject(:+) || 0),
                # solicitudes cerradas
                (requests.map(&:id) & closed_requests.map(&:id)).size,
                # solicitudes sin trámite
                (requests.map(&:id) & no_process_requests.map(&:id)).size,
                # solicitudes en trámite
                (requests.map(&:id) & process_requests.map(&:id)).size,


                # requerimientos recibidos
                (process_requirements + records.map(&:total_requirements).compact.inject(:+) || 0),
                #(requirements.map(&:weight).inject(:+) || 0) + process_requirements + (records.map(&:total_requirements).compact.inject(:+) || 0),
                # requerimientos oficiosos ???????
                #((requirements.select{|o| o.informal?}).map(&:weight).inject(:+) || 0) + (records.map(&:art_10_17_laip).inject(:+) || 0),
                # requerimientos públicos
                ((requirements.select{|o| o.public?}.size || 0) + records.map(&:no_proprietary).inject(:+) || 0),
                # no entregados por ser confidenciales
                ((requirements.select{|o| o.denied_confidential?}.size || 0) + records.map(&:art_24_laip).inject(:+) || 0),
                # datos personales
                ((requirements.select{|o| o.personal_data?}.size || 0) + records.map(&:art_31_32_laip).inject(:+) || 0),
                # no entregados por ser reservados
                ((requirements.select{|o| o.denied_reserved?}.size || 0) + records.map(&:art_19_laip).inject(:+) || 0),
                # versiones públicas
                ((requirements.select{|o| o.public_version?}.size || 0) + records.map(&:public_version).inject(:+) || 0),
                # inexistentes
                ((requirements.select{|o| o.non_existent?}.size || 0) + records.map(&:art_73_laip).inject(:+) || 0),
                # desestimados
                ((requirements.select{|o| o.undelivered?}.size || 0) + records.map(&:dismissed_requirements).inject(:+) || 0),
                # re-direccionados
                ((requirements.select{|o| o.redirected?}.size || 0) + records.map(&:art_67_laip).inject(:+) || 0),
                # sin trámite
                ((requirements.select{|o| o.not_processed?}.size || 0) + records.map(&:art_74_laip).inject(:+) || 0),
                # en proceso
                process_requirements + (records.map(&:process_requirements).inject(:+) || 0),
                # average response time requests
                average_response_requests.size == 0 ? 0 : ((average_response_requests.inject(:+).to_f / average_response_requests.size).round(2) rescue 0),
                # min response time request
                average_response_requests.min,
                # max response time request
                average_response_requests.max,
                # antiquity requests
                requests.select{|r| r.extension_type.to_s == 'antiquity'}.size,
                # complexity requests
                requests.select{|r| r.extension_type.to_s == 'complexity'}.size,
                # antiquity_and_complexity requests
                requests.select{|r| r.extension_type.to_s == 'antiquity_and_complexity'}.size,
                # women requests
                requests.select{|r| r.gender.to_s == 'Femenino'}.size,
                # men requests
                requests.select{|r| r.gender.to_s == 'Masculino'}.size
              ]
            end
          end
        end
      ### VISTA DETALLE POR REQUERIMIENTOS
      elsif view.to_s == 'requirement'
        ### PREPARE CSV HEADER
        csv_string = CSV.generate do |csv|
          csv << ["Estadísticas avanzadas SGS #{Date.current.try(:strftime, '%d-%m-%Y')} - Detalle por requerimientos"]
          csv << [
                    'Institución',
                    'Correlativo',
                    'Fecha de ingreso',
                    'Forma de ingreso',
                    'Solicitante',
                    'Correo electrónico',
                    'Teléfono',
                    'Celular',
                    'Tipo de persona',
                    'Nivel de estudio',
                    'Profesión',
                    'Fecha de admisión',
                    #'10 días',
                    'Fecha resolución',
                    #'Cantidad',
                    'Tipo de información',
                    'Tipo de resolución',
                    'Unidad administrativa',
                    'Correo del enlace de la unidad',
                    'Notificación',
                    'Entrega',
                    'Complejidad de la información',
                    'Justificación de prórroga',
                    'Días por requerimiento',
                    'Días por solicitud',
                    'Edad',
                    'Sexo',
                    'Nacionalidad',
                    'Residencia',
                    'Departamento',
                    'Municipio',
                    'Resumen'
                ]
          ### PREPARE GLOBAL VARS
          requests = []
          institutions = params[:institution_id].present? ? Institution.order(:name).where(id: params[:institution_id]) : Institution.order(:name)
          ### PREPARE DATA
          puts "-------#{params[:from_system]}"
          if params[:from_system].present?
            if current_user.has_role?("admin")
              system_requests = model.confirmed
            else
              system_requests = model.where("institution_id IN (?)", current_user.institution_ids)
            end
            system_requests = system_requests.where('information_requests.admitted_at > ?', Date.parse(params[:system_start_at]).beginning_of_day) if params[:system_start_at].present?
            system_requests = system_requests.where('information_requests.admitted_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
            system_requests = system_requests.where(institution_id: params[:institution_id]) if params[:institution_id].present?
          end
          ### EXPORT ROWS

          institutions.each do |institution|
            # system requests
            requests = system_requests.select{|o| o.institution_id == institution.try(:id)}
            unless requests.blank?
              # managements in the period
              requests.each do |request|
                requirements = Requirement.where("information_request_id = ?", request.id)
                requirements.each do |requirement|
                  csv << [
                    # institución
                    institution.nil? ? 'No se donde enviarla' : institution.name,
                    # correlativo
                    request.correlative,
                    # fecha de ingreso
                    request.confirmed_at.try(:strftime, '%d/%m/%y'),
                    # forma de ingreso
                    request.from_website? ? 'Sitio web' : 'Manual',
                    # solicitante
                    request.fullname,
                    # correo electrónico
                    request.email,
                    # teléfono
                    request.phone,
                    # celular
                    request.cellular,
                    # tipo de persona
                    request.entity_type.present? ? I18n.t("activerecord.enum.information_request.entity_type.#{request.entity_type}") : '',
                    # nivel de estudio
                    request.educational_level.present? ? I18n.t("activerecord.enum.information_request.educational_level.#{request.educational_level}") : '',
                    # profesión
                    request.occupation.try(:name),
                    # fecha de admisión
                    request.admitted_at.try(:strftime, '%d/%m/%y'),
                    # 10 días
                    #request.start_date_with_business(10, request.admitted_at).try(:strftime, '%d/%m/%y'),
                    # fecha de resolución
                    requirement.updated_at.try(:strftime, '%d/%m/%y'),
                    # Cantidad
                    #requirement.weight,
                    # tipo de información
                    requirement.get_information_type,
                    # tipo de resolución
                    requirement.get_resolution_type,
                    # unidades administrativas
                    requirement.show_assigned_users_name,
                    # correo de los enlaces de las unidades administrativas
                    requirement.show_assigned_users,
                    # notificación
                    request.notification_mode.present? ? request.notification_mode_string : '',
                    # entrega
                    request.delivery_way.present? ? request.delivery_way : '',
                    # prórroga
                    request.extension_type,
                    # justificación prórroga
                    request.events.where(state: "extended").present? ? request.events.where(state: "extended").first.justification : '',
                    # días por requerimiento
                    requirement.process_days,
                    # días por solicitud
                    request.process_days,
                    # edad
                    request.try(:strftime, '%d/%m/%y'),
                    # sexo
                    request.gender.present? ? request.gender : '',
                    # nacionalidad
                    request.foreign? ? request.nationality_name : 'Salvadoreña',
                    # residencia
                    request.el_salvador? ? 'El Salvador' : 'Otra',
                    # departamento
                    (request.city.present? and request.city.state.present?) ? request.city.state.name : '',
                    # municipio
                    request.city.present? ? request.city.name : '',
                    # resumen
                    #{}"#{requirement.comments.last.message.gsub('"', '\'').truncate(80)}"
                  ]
                end
              end
            end
          end
        end
      else
        ### VISTA COMPILADA
        csv_string = CSV.generate do |csv|
          csv << ["Estadísticas avanzadas SGS #{Date.current.try(:strftime, '%d-%m-%Y')} - Compilado"]
          csv << [
                    'Institución',
                    'Solicitud de información',
                    'Requerimientos de información',
                    'Respondido completamente',
                    'Respondido parcial',
                    'Respondido reserva total',
                    'Respondido no procedente',
                    'No se dio trámite',
                    'En trámite',
                    'Total de respuestas entregadas',
                    'Otros: Quejas y avisos',
                    'Total de procedimientos'
                ]
          requests = InformationRequest.confirmed.accessible_by(current_ability)
          requests = requests.where('information_requests.admitted_at > ?', Date.parse(params[:system_start_at]).beginning_of_day) if params[:system_start_at].present?
          requests = requests.where('information_requests.admitted_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
          requests = requests.where(institution_id: params[:institution_id]) if params[:institution_id].present?
          requests.group_by{|r| r.institution}.each do |institution, requests|
            requirements = Comment.requirements.where(commentable_type: 'InformationRequest', commentable_id: requests.map(&:id)).pluck(:id)
            closed = InformationRequestEvent.where(eventable_type: 'Comment', eventable_id: requirements, state: 'closed')
            closed = closed.where("created_at <= ?", Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
            closed = closed.map(&:eventable).uniq
            if institution
              complaints = institution.complaints.where(confirmed: true)
              complaints = complaints.where('complaints.updated_at > ?', Date.parse(params[:system_start_at]).beginning_of_day) if params[:system_start_at].present?
              complaints = complaints.where('complaints.updated_at < ?', Date.parse(params[:system_end_at]).end_of_day) if params[:system_end_at].present?
            else
              complaints = []
            end
            csv << [
              institution.nil? ? 'No se donde enviarla' : institution.name,
              requests.size,
              requirements.size,
              closed.select{|r| r.complete?}.size,
              closed.select{|r| r.partial?}.size,
              closed.select{|r| r.total_reserve?}.size,
              closed.select{|r| r.not_appropiate?}.size,
              closed.select{|r| r.not_processed?}.size,
              requirements.size - closed.size,
              closed.size,
              complaints.size,
              closed.size + complaints.size
            ]
          end
        end
      end
      send_data csv_string, type: 'text/csv; charset=utf-8; header=present;', filename: "Estadisticas-avanzadas-#{Date.current.try(:strftime, '%Y%m%d')}.csv", disposition: 'attachment'
    end

    def json_methods
      [:remaining_days, :percent, :tags_string]
    end

    private

    def institutions
      @institutions = current_user.has_role?("admin") ?
      Institution.all.order(:name) :
      Institution.where("id IN (?)", current_user.institution_ids).order(:name)
    end
    helper_method :institutions

    def information_request
      @information_request ||= params[:id] ?
      InformationRequest.find(params[:id]) :
      InformationRequest.new(params[:information_request])
    end
    helper_method :information_request

    def init_form
      @occupations = Occupation.order(:name)
      @states = State.order(:name)
    end

    def nationalities
      @nationalities ||= Nationality.order(:name)
    end
    helper_method :nationalities

    def breadcrumbs_for(action, information_request=nil)
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(count: :many), index_url
      add_breadcrumb "Solicitud de información personal", new_personal_panel_information_requests_url if action_name == "new_personal" || action_name == "create_personal"
      add_breadcrumb "Editar solicitud de información personal", edit_personal_panel_information_request_url if action_name == "edit_personal" || action_name == "update_personal"
      add_breadcrumb information_request.correlative, show_url if action_name == "closure"
      add_breadcrumb t(action.to_s), nil
    end


    def prepare_data
      if current_user.has_role?("admin")
        requests = model.confirmed
      else
        requests = model.where("institution_id IN (?)", current_user.institution_ids)
      end
      #@start_date_at = Date.civil(*params[:start_at].sort.map(&:last).map(&:to_i)).beginning_of_day rescue nil
      #@end_date_at = Date.civil(*params[:start_at].sort.map(&:last).map(&:to_i)).end_of_day rescue Date.today.end_of_day
      @start_date_at = Date.parse(params[:start_at]).beginning_of_day rescue nil
      @end_date_at = Date.parse(params[:end_at]).end_of_day rescue Date.today.end_of_day
      requests = requests.where('information_requests.admitted_at > ?', @start_date_at) unless @start_date_at.nil?
      requests = requests.where('information_requests.admitted_at < ?', @end_date_at) unless @end_date_at.nil?
      requests2 = requests
      @total_requests = requests.count
      @new_requests = requests.newer.size
      @correct_requests = requests.correct.size
      @process_requests = requests.process_requests.size
      @expired_requests = requests.expired(@end_date_at).size
      @closed_requests = requests.closed_requests.size
      @not_admitted_requests = requests2.not_admitted_requests.size
      @website_requests = requests.select{|r| r.from_website?}.size
      @manual_requests = requests.select{|r| !r.from_website?}.size

      ### requirements
      requirements = Requirement.where(information_request_id: requests.pluck(:id))

      @process_requirements = requirements.in_process.size
      @closed_requirements = requirements.are_closed.size
      @total_requirements = (@closed_requirements || 0) + (@process_requirements || 0)

      ### information type
      @public = requirements.where(information_type: 'public').count
      @reserved = requirements.where(information_type: 'reserved').count
      @confidential = requirements.where(information_type: 'confidential').count
      @non_existent = requirements.where(information_type: 'non_existent').count
      @incompetent = requirements.where(information_type: 'incompetent').count
      @undelivered = requirements.where(information_type: 'dismissed').count
      @not_processed = requirements.where(information_type: 'not_processed').count
      @total_information = requirements.count

      ### graphic resume
      @title_information_type = t('panel.information_requests.statistics.type_of_information_provided')
      @data_information_type = [ [t('panel.information_requests.statistics.information_type'), t('panel.information_requests.statistics.total')], [t('panel.information_requests.statistics.public'), @public], [t('panel.information_requests.statistics.reserved'), @reserved], [t('panel.information_requests.statistics.confidential'), @confidential], [t('panel.information_requests.statistics.non_existent'), @non_existent], [t('panel.information_requests.statistics.incompetent'), @incompetent], [t('panel.information_requests.statistics.undelivered'), @undelivered], [t('panel.information_requests.statistics.not_processed'), @not_processed] ]

      ### graphic demography
      @title_gender = t('panel.information_requests.statistics.requests_by_gender')
      @data_gender = requests.group_by{|r| r.gender}.map{|k, v| [t("panel.information_requests.statistics.#{k}"), v.size] }
      @data_gender.unshift([t('panel.information_requests.statistics.gender'), t('panel.information_requests.statistics.total')])

      @title_person = t('panel.information_requests.statistics.requests_by_person_type')
      @data_person = requests.group_by{|r| r.entity_type}.map{|k, v| [t("panel.information_requests.statistics.#{k}"), v.size] }
      @data_person.unshift([t('panel.information_requests.statistics.person_type'), t('panel.information_requests.statistics.total')])

      ### occupations
      @title_occupation  = t('panel.information_requests.statistics.requests_by_occupation')
      @data_occupation = requests.group_by{|r| r.occupation}.map{|k, v| [k.name, v.size] }
      @data_occupation.unshift([t('panel.information_requests.statistics.occupation'), t('panel.information_requests.statistics.total')])

      ### age
      #@title_age = t('panel.information_requests.statistics.requests_by_age')
      #@data_age = requests.where.not(birthday: nil)
      #@data_age2 = @data_age.group_by{|r| r.birthday.year}.map{|k, v| [(Date.today.year - k).to_s, v.size]}
      #@data_age = [ [t('panel.information_requests.statistics.age'), t('panel.information_requests.statistics.total')], [t('panel.information_requests.statistics.less_than_18'), requests.where('birthday BETWEEN ? AND ?', (Date.today - 18.years).beginning_of_year, Date.today.end_of_year).count], [t('panel.information_requests.statistics.between_18_and_30'), requests.where('information_requests.age BETWEEN 18 AND 30').count], [t('panel.information_requests.statistics.between_31_and_50'), requests.where('information_requests.age BETWEEN 31 AND 50').count], [t('panel.information_requests.statistics.greater_than_50'), requests.where('information_requests.age > 50').count] ]
      #puts "---------#{@data_age.inspect}"
      #@data_age.unshift([t('panel.information_requests.statistics.age'), t('panel.information_requests.statistics.total')])

      #@title_age = t('panel.information_requests.statistics.requests_by_age')
      #@data_age = [ [t('panel.information_requests.statistics.age'), t('panel.information_requests.statistics.total')], [t('panel.information_requests.statistics.less_than_18'), requests.where('information_requests.age < 18').count], [t('panel.information_requests.statistics.between_18_and_30'), requests.where('information_requests.age BETWEEN 18 AND 30').count], [t('panel.information_requests.statistics.between_31_and_50'), requests.where('information_requests.age BETWEEN 31 AND 50').count], [t('panel.information_requests.statistics.greater_than_50'), requests.where('information_requests.age > 50').count] ]
      @title_department = t('panel.information_requests.statistics.requests_by_department')
      @data_department = requests.group_by{|r| (r.city.state.name rescue I18n.t('label.none'))}.sort_by{|k, v| k}.map{|k, v| [k, v.size] }
      @data_department.unshift([t('panel.information_requests.statistics.department'), t('panel.information_requests.statistics.total')])
      ### graphic time
      @title_time = t('panel.information_requests.statistics.requests_by_time')
      @data_time = requests.group_by{|r| r.created_at.to_date}.sort_by{|k, v| k}.map{|k,v| [I18n.l(k, format: :short), v.size]}
      @data_time.unshift([t('panel.information_requests.statistics.day'), t('panel.information_requests.statistics.total')])
    end


  end
end
