# frozen_string_literal: true
module Panel
  #
  class UsersController < PanelController
    before_action :check_admin
    include Panel::Crud
    include Panel::Multiple


    def model
      User
    end

    def permits
      [:name, :email, :password, :password_confirmation, :administrative_unit_name,
        institution_ids: [], :role_attributes => [:name]
      ]

    end

    def init_form
      if current_user.has_role?("admin")
        @institutions = Institution.order(Institution.acts_as_label)
        @allowed_roles = model.roles.keys
      else
        @institutions = Institution.where("id IN (?)", current_user.institution_ids)
        @allowed_roles = model.roles.keys.drop(1)
      end
      action_name == "edit" ? @roles = User.find(params[:id]).roles.first : @roles = nil
    end

    def respond_to_json
      @q = current_user.has_role?("admin") ? User : User.without_admin_role.joins(:institutions).where("institutions.id IN (?)", current_user.institution_ids)
      #User.without_admin_and_oir_role.joins(:institutions).where("institutions.id IN (?)", current_user.institution_ids)
      @q = @q.ransack(params[:filter])
      @c = @q.result.paginate(page: params[:page], per_page: params[:count])

      render json: json_result(@c)
    end

    def create
      @item = model.new item_params
      set_uniq_institution!
      if @item.save
        @item.add_role(params[:role])
        return redirect_to(edit_url, flash: { saved: true })
      end
      init_form
      breadcrumbs_for(:new)
      render template: 'concerns/panel/form'
    end

    def edit
      @item = User.find params[:id]
      init_form
      breadcrumbs_for(:edit)
      render template: 'concerns/panel/form'
    end

    def update

      @item.attributes = item_params
      m = @item.password.blank? ? :update_without_password : :update_attributes

      if @item.send(m, item_params)
        if @item.roles.blank?
          @item.add_role(params[:role])
        else
          @item.roles = []
          @item.add_role(params[:role])
        end
        return redirect_to(edit_url, flash: { saved: true })
      end


      breadcrumbs_for(:edit)
      init_form
      render template: 'concerns/panel/form'
    end

    private

    def check_admin
      if current_user.has_role?("administrative_unit")
        redirect_to panel_root_url
      end
    end

  end
end
