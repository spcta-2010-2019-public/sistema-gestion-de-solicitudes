# frozen_string_literal: true
module Panel
  #
  class InformationRequestRecordsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    load_and_authorize_resource

    def model
      InformationRequestRecord
    end

    def permits
      [ :opening_date, :closing_date, :institution_id, :user_id, :total_requests,
        :total_requirements, :dismissed_requirements, :abandoned_requirements,
        :art_10_17_laip, :no_proprietary, :art_31_32_laip, :art_30_laip,
        :art_30_laip_reserved, :art_30_laip_confidential, :art_19_laip,
        :art_24_laip, :art_73_laip, :art_74_laip, :art_67_laip,
        :process_requirements, :other ]
    end

    def exportable_fields
      [:opening_date, :closing_date, :institution_id, :user_id, :total_requests,
        :total_requirements, :dismissed_requirements, :abandoned_requirements,
        :art_10_17_laip, :no_proprietary, :art_31_32_laip, :art_30_laip,
        :art_30_laip_reserved, :art_30_laip_confidential, :art_19_laip,
        :art_24_laip, :art_73_laip, :art_74_laip, :art_67_laip,
        :process_requirements, :other ]
    end

    def index
      respond_to do |format|
        format.html { super }
        format.json { respond_to_json }
      end
    end

    def respond_to_json
      if current_user.admin?
        @q = model
      elsif current_user.oir?
        @q = model.where("institution_id IN (?)", current_user.institution_ids)
      else
        @q = nil
      end
      @q = @q.ransack(params[:filter])
      @c = @q.result.paginate(page: params[:page], per_page: params[:count])
      render json: json_result(@c)
    end

    def create
      @item = model.new item_params
      @item.user_id = current_user.id
      if @item.valid_totals? and @item.valid_requirements?
        if @item.save
          return redirect_to(index_url, flash: { saved: true })
        else
          init_form
          breadcrumbs_for(:new)
          render template: 'concerns/panel/form'
        end
      else
        init_form
        breadcrumbs_for(:new)
        render template: 'concerns/panel/form'
      end
    end

    def update
      @item.assign_attributes(item_params)
      respond_to do |format|
        format.json { super }
        format.html do
          if @item.valid_totals? and @item.valid_requirements?
            return redirect_to(edit_url, flash: { saved: true }) if
              @item.update_attributes(item_params)
          else
            breadcrumbs_for(:edit)
            init_form
            render template: 'concerns/panel/form'
          end
        end
      end
    end

    def show
      @item = InformationRequestRecord.find(params[:id]).decorate
      breadcrumbs_for(:show)
    end

    def institutions
      @institutions = current_user.has_role?("admin") ?
      Institution.all.order(:name) :
      Institution.where("id IN (?)", current_user.institution_ids).order(:name)
    end
    helper_method :institutions

    def breadcrumbs_for(action, information_request=nil)
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(count: :many), index_url
      #add_breadcrumb information_request.correlative, show_url if action_name == "closure"
      add_breadcrumb t(action.to_s), nil
    end

  end
end
