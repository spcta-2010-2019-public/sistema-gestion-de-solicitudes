# frozen_string_literal: true
module Panel
  #
  class EventsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      Event
    end

    def permits
      [:justification, :event_start, :document, :user_id ]
    end

    def exportable_fields
      [:name]
    end

    def create
      @item = model.new item_params
      @item.requirements.build unless @item.requirements.any?
      @item.notification_mode = params[:notification_mode] || []
      @item.from_website = false
      @item.confirmed = true
      @item.confirmed_at = Time.current

      if @item.save
        @item.state = "newer"
        #@item.set_reference_code
        #@item.set_correlative

        #@item.delivery_deadline = @information_request.start_date_with_business(10)
        @item.save

        return redirect_to(index_url, flash: { saved: true })
      else
        puts @item.errors.full_messages
        init_form
        breadcrumbs_for(:new)
        render template: 'concerns/panel/form'
      end
    end

    def show
      @item = InformationRequest.find(params[:id]).decorate
      breadcrumbs_for(:show)
      #@allowed_users = User.joins(:roles).where("")
    end

    def get_states
      render json: @states = InformationRequest.states.to_json
    end

    def not_admitted

    end

    def admitted
      @item = InformationRequest.find(params[:id])
      @item.set_reference_code
      @item.set_correlative
      @item.admitted_at = Time.current
      #@item.delivery_deadline = @item.start_date_with_business(10)
      #@item.deliver_information_request_user_notification if information_request.save(validate: false)
      @item.save(validate: false)
      redirect_to panel_information_request_url(@item.id)
    end

    private

    def institutions
      @institutions = current_user.has_role?("admin") ?
      Institution.all.order(:name) :
      Institution.where("id IN (?)", current_user.institution_ids).order(:name)
    end
    helper_method :institutions

    def information_request
      @information_request ||= params[:id] ?
      InformationRequest.find(params[:id]) :
      InformationRequest.new(params[:information_request])
    end
    helper_method :information_request

    def init_form
      @occupations = Occupation.order(:name)
      @states = State.order(:name)
    end

    def nationalities
      @nationalities ||= Nationality.order(:name)
    end
    helper_method :nationalities


  end
end
