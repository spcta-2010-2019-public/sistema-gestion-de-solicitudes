# frozen_string_literal: true
module Panel
  #
  class CalendarEventsController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      CalendarEvent
    end

    def new
      @institution_calendar = InstitutionCalendar.find(params[:institution_calendar_id])
      @item = model.new

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/calendar_events/form'
    end

    def edit
      @item = CalendarEvent.find(params[:id])
      @institution_calendar = @item.institution_calendar

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/calendar_events/form'
    end

    def create
      @item = model.new item_params
      @item.user_id = current_user.id
      #@item.institution_calendar_id = params[:institution_calendar_id]
      return redirect_to(edit_panel_institution_calendar_calendar_event_url(@item.institution_calendar_id, @item.id), flash: { saved: true }) if @item.save

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/calendar_events/form'
    end

    def update
      @item = CalendarEvent.find(params[:id])
      @institution_calendar = @item.institution_calendar
      return redirect_to(edit_panel_institution_calendar_calendar_event_url(@item.institution_calendar_id, @item.id), flash: { saved: true }) if @item.update_attributes(item_params)
      init_form
      breadcrumbs_for(:new)
      render template: 'panel/calendar_events/form'
    end

    def destroy
      @item = CalendarEvent.find(params[:id])
      @institution_calendar = @item.institution_calendar
      @item.destroy
      redirect_to panel_institution_calendar_url(@institution_calendar), flash: { destroyed: true }
    end

    def permits
      [:name, :description, :kind, :event_date, :repetitive, :institution_calendar_id]
    end

    def exportable_fields
      [:name]
    end

    def respond_to_html
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(:many), index_url
      render template: 'panel/calendar_events/index'
    end

    def breadcrumbs_for(action)
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb "Listado de eventos", panel_institution_calendar_url(params[:institution_calendar_id])
      add_breadcrumb t(action.to_s), nil
    end

  end
end
