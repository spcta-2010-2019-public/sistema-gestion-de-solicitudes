# frozen_string_literal: true
module Panel
  #
  class CommentsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    load_and_authorize_resource
    skip_before_filter :verify_authenticity_token
    layout false
    respond_to :html, :js

    def model
      Comment
    end

    def permits
      [:message, :document, :requirement_id]
    end

    def exportable_fields
      [:message, :document]
    end

    def new
    end

    def create
      @requirement = Requirement.find(params[:item][:requirement_id])
      @item = model.new item_params
      @item.user_id = current_user.id
      if @item.save
        @users = User.where(id: @requirement.assigned_users) rescue nil
        @users = @users - [current_user] rescue nil
        if @users
          @users.each do |user|
            UserMailer.delay.new_information_request_reply_to_comment(current_user, @item, @requirement, user)
            #UserMailer.new_information_request_reply_to_comment(current_user, @item, @requirement, user).deliver
          end
        end
      else
        @error = "¡Ha ocurrido un error!"
      end
      respond_to do |format|
        format.js
      end
    end


  end
end
