# frozen_string_literal: true
module Panel
  #
  class HomeController < PanelController
    def index
    end
  end
end
