# frozen_string_literal: true
module Panel
  #
  class InstitutionSchedulesController < PanelController
    include Panel::Crud
    include Panel::Multiple

    before_action :set_uniq_institution!, only: [:create, :update]


    def model
      InstitutionSchedule
    end

    def respond_to_json
      if current_user.admin?
        @q = model
      elsif current_user.oir?
        @q = model.where(institution_id: current_user.institution_ids)
      end
      @q = @q.ransack(params[:filter])
      @c = @q.result.paginate(page: params[:page], per_page: params[:count])
      render json: json_result(@c)
    end

    def init_form
    end

    def institutions
      @institutions = current_user.has_role?("admin") ?
      Institution.all.order(:name) :
      Institution.where("id IN (?)", current_user.institution_ids).order(:name)
    end
    helper_method :institutions

    def permits
      [:wday, :from, :to, :institution_id]
    end
  end
end
