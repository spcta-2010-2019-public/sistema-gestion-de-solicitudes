# frozen_string_literal: true
module Panel
  #
  class InformationRequestIaipProcessNotesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    load_and_authorize_resource
    skip_before_filter :verify_authenticity_token
    layout false
    respond_to :html, :js

    def model
      InformationRequestIaipProcessNote
    end

    def permits
      [:content, :note_date, :information_request_iaip_process_id]
    end

    def new
    end

    def create
      @item = model.new item_params
      if @item.save
      else
        @error = "¡Ha ocurrido un error!"
      end
      respond_to do |format|
        format.js
      end
    end


  end
end
