# frozen_string_literal: true
module Panel
  #
  class InformationRequestIaipProcessesController < PanelController
    include Panel::Crud
    include Panel::Multiple

    def model
      InformationRequestIaipProcess
    end

    def respond_to_html
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb humanized_model_name(:many), index_url
      render template: 'panel/information_request_iaip_processes/index'
    end

    def respond_to_json
      if current_user.admin?
        @q = InformationRequest.closed_requests
      elsif current_user.oir?
        @q = InformationRequest.where("institution_id IN (?)", current_user.institution_ids).closed_requests
      end
      @q = @q.ransack(params[:filter])
      @c = @q.result.paginate(page: params[:page], per_page: params[:count])

      render json: json_result(@c)
    end

    def json_methods
      [:new_or_existing, :boolean_new_or_existing]
    end

    def new
      @information_request = InformationRequest.find(params[:id])
      @item = model.new
      @item.notes.build

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/information_request_iaip_processes/form'
    end

    def show
      @item = model.find(params[:id]).decorate
      breadcrumbs_for(:show)
      render template: 'panel/information_request_iaip_processes/show'
    end

    def create
      @item = model.new item_params
      return redirect_to(show_url, flash: { saved: true }) if @item.save
      puts @item.errors.full_messages

      init_form
      breadcrumbs_for(:new)
      render template: 'panel/information_request_iaip_processes/form'
    end

    def edit
      init_form
      breadcrumbs_for(:edit)

      render template: 'panel/information_request_iaip_processes/form'
    end

    def update
      respond_to do |format|
        format.json { super }
        format.html do
          return redirect_to(edit_url, flash: { saved: true }) if
            @item.update_attributes(item_params)

          breadcrumbs_for(:edit)
          init_form
          render template: 'panel/information_request_iaip_processes/form'
        end
      end
    end

    def permits
      [:presentation_date, :notification_date, :correlative, :process_type,
      :information_request_id,
      :description, :resolution_result, :resolution_phase, :fullfillment_date,
      :comments, notes_attributes: [:id, :note_date, :content, :_destroy],
      attachments_attributes: [:id, :information_request_iaip_process_id,
      :title, :file, :description, :_destroy]]
    end

    def exportable_fields
      [:name]
    end

  end
end
