# frozen_string_literal: true
module Panel
  #
  class RequirementsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    load_and_authorize_resource
    layout false
    respond_to :js


    def model
      Requirement
    end

    def permits
      [:information_request_id, :user_id, :requested_information, :state,
      :file, :information_type, :resolution_type, :meets_depth, :art_74_laip,
      :assigned_users, :invited_users]
    end

    def exportable_fields
      [:information_request_id, :user_id, :requested_information, :state,
      :file, :information_type, :resolution_type, :meets_depth, :art_74_laip,
      :assigned_users, :invited_users]
    end

    def new
    end

    def create
      @information_request = InformationRequest.find(params[:requirement][:information_request_id])
      @requirement = Requirement.new(requirement_params)
      if @requirement.save
        return redirect_to(panel_information_request_url(@information_request), flash: { saved: true })
      else
        return redirect_to(panel_information_request_url(@information_request), flash: { error_requirement: true })
      end
    end

    def management
      @requirement = Requirement.find(params[:id])
      requirement_params = item_params

      @information_request = InformationRequest.find(@requirement.information_request_id)
      if params[:assigned_users] or params[:invited_users]
        @requirement[:assigned_users] = Array.new(params[:assigned_users]) if params[:assigned_users]
        @requirement[:invited_users] = Array.new(params[:invited_users]) if params[:invited_users]
        # Validacion para fecha de actualizacion de asignados, para requerimientos previos al haberse agregado estos campos
        if @requirement.has_assigned_users?
          if @requirement.assigned_date.nil?
            @requirement[:assigned_date] = @requirement.events.last.event_start rescue @requirement.updated_at
          end
          if @requirement.assigned_date_update.nil?
            @requirement[:assigned_date_update] = @requirement.events.last.event_start rescue @requirement.updated_at
          end
        end

        if @requirement.assigned_users_changed?
          @sent_assigned_mail = true
          if @requirement.assigned_date.nil?
            @requirement[:assigned_date] = Time.now
          end
          @requirement[:assigned_date_update] = Time.now
        end
        if @requirement.invited_users_changed?
          @sent_invited_mail = true
        end
      end

      @requirement[:user_id] = current_user.id
      if params[:item][:art_74_laip] == "true"
        requirement_params[:information_type]= "public"
        requirement_params[:resolution_type] = "art_74_b_laip"
      end

      if @requirement.update_attributes requirement_params
        if @sent_assigned_mail && !@requirement.assigned_users.blank?
          @users = User.where(id: @requirement.assigned_users) rescue nil
          #@officer = User.where(id: @requirement.user_id).first rescue nil
          #@users = @users.to_a << @officer unless @officer.blank?
          @users = @users - [current_user] rescue nil
          if @users
            @users.each do |user|
              UserMailer.delay.new_information_request_comment_to_user(current_user, @requirement, user)
              #UserMailer.new_information_request_comment_to_user(current_user, @requirement, user).deliver
            end
          end
        end
        if @sent_invited_mail && !@requirement.invited_users.blank?

          @users = User.where(id: @requirement.invited_users) rescue nil
          @users = @users - [current_user] rescue nil
          if @users
            @users.each do |user|
              UserMailer.delay.new_information_request_comment_invite_user(current_user, @requirement, user)
            end
          end
        end
        @event ||= @requirement.events.last
        # Hacerlo en modelo?
        if @requirement.art_74_laip == true || ((params[:close_requirement] == "true" && !@requirement.resolution_type.nil?))

          @requirement.state = "closed"
          unless @event.nil? or @event.state.to_s == "closed_requirement"
            @event = @requirement.events.new
            @event.event_start = Time.now
            @event.user_id = current_user.id
            @event.justification = @requirement.art_74_laip == true ? "Se le da cierre al requerimiento por Articulo 74-B." : params[:justification]
            @event.document = params[:document]
            @event.save!
          end
          @requirement.save!

        elsif @requirement.art_74_laip == false && !@requirement.meets_depth.nil? && @requirement.newer?
          @requirement.state = "process"
          @event = @requirement.events.build
          @event.event_start = Time.now
          @event.user_id = current_user.id
          @event.justification = "Requerimiento en proceso."
          @requirement.save!
          @event.save!
        end
      else
        @error = "¡Ha ocurrido un error!"
      end
      respond_to do |format|
        format.js
      end

    end

    def destroy
      @item = Requirement.find(params[:id])
      @information_request = InformationRequest.find(@item.information_request_id)
      @item.destroy
      return redirect_to(panel_information_request_url(@information_request), flash: { destroyed: true })
    end

    private

    def requirement_params
      perms = params.require(:requirement).permit!
      perms

    end

    def init_form
      @occupations = Occupation.order(:name)
      @states = State.order(:name)
    end

    def nationalities
      @nationalities ||= Nationality.order(:name)
    end
    helper_method :nationalities

  end
end
