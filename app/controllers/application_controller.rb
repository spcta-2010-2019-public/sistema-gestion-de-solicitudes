class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    return panel_root_url if resource_or_scope == :admin
    super
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404.html", :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end


end
