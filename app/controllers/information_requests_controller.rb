# frozen_string_literal: true
#
class InformationRequestsController < ApplicationController

  def permits
    [:name, :institution_id, :nationality, :nationality_name, :firstname,
    :lastname, :email, :document_type, :document_number, :entity_type,
    :document_front_image, :document_back_image, :other_document, :terms,
    :birthday, :gender, :phone, :cellular, :fax, :educational_level,
    :residence, :address, :state_id, :city_id, :occupation_id, :delivery_way,
    :notification_mode, :reference_code, :confirmed, :confirmed_at,
    :admitted_at, :delivery_deadline, :extension_type, :resolution_type,
    :correlative, :redirected_institution_id, :seen_document, :ignore_it,
    :type_of_rights, :requested_personal_info, :request_type,
    :from_website, :resource, :state, :meets_shape, :legal_representative,
    :legal_representative_doc, :reception, tag_list: [],
    requirements_attributes: [:id, :requested_information, :_destroy],
    events_attributes: [:id, :justification, :event_start, :document, :user_id,
    :state]]
  end


  def index
    redirect_to new_information_request_url and return
  end

  def new
    initialize_form
  end

  def create
    @item = InformationRequest.new item_params
    @item.requirements.build unless @item.requirements.any?
    @item.notification_mode = params[:notification_mode] || []
    @item.from_website = true
    @item.reception = "website"
    @item.state = "newer"

    #@item.confirmed = true
    #@item.confirmed_at = Time.current
    #@item.terms = true
    @success = verify_recaptcha(model: @item, message: "Marque la opción 'No soy un robot'")
    if @success && @item.save


      @item.deliver_confirmation_email_request
      session[:information_request_id] = @item.id
      redirect_to please_confirm_information_requests_path()
    else
      init_form
      render :new
    end
  end

  def create_personal
    @item = InformationRequest.new item_params
    @item.requirements.build unless @item.requirements.any?
    @item.notification_mode = params[:notification_mode] || []
    @item.type_of_rights = params[:type_of_rights] || ["access"] #Si no hay ninguno chequeado
    @item.from_website = true
    @item.reception = "website"
    @item.state = "newer"

    #@item.confirmed = true
    #@item.confirmed_at = Time.current
    #@item.terms = true
    @success = verify_recaptcha(model: @item, message: "Marque la opción 'No soy un robot'")
    if @success && @item.save

      @item.deliver_confirmation_email_personal_request

      session[:information_request_id] = @item.id
      redirect_to please_confirm_information_requests_path()
    else
      init_form
      render :new_personal
    end
  end


  def new_personal
    initialize_form
  end

  def please_confirm

    if session[:information_request_id]
      @information_request = InformationRequest.find(session[:information_request_id])
      session.delete :information_request_id
    else
      redirect_to information_requests_url
    end
  end

  def confirm
    unless params[:token].blank?
      @information_request = InformationRequest.where(token: params[:token]).first rescue nil
    end
    unless @information_request.blank?
      #Mandar correo
      @information_request.confirmed = true
      @information_request.confirmed_at = Time.current
      @information_request.token = nil
      @information_request.save
      #begin
        #token = params[:token]
        #decoded_token = JWT.decode token, rsa_public, true, { :algorithm => 'RS256' }
        #puts decoded_token.first
        # continue with your business logic
      #rescue JWT::ExpiredSignature
        # Handle expired token
        # inform the user his invite link has expired!
        #puts "Token expired"
      #end
      if @information_request.request_type == "normal"
        @information_request.deliver_information_request
      else
        @information_request.deliver_personal_information_request
      end
    else
      render_404
    end
  end

  private

  def initialize_form
    @item = InformationRequest.new
    @item.requirements.build
    init_form
  end

  def institutions
    @institutions = Institution.is_enabled.order(:name)
  end
  helper_method :institutions

  def information_request
    @information_request ||= params[:id] ?
    InformationRequest.find(params[:id]) :
    InformationRequest.new(params[:information_request])
  end
  helper_method :information_request

  def init_form
    @occupations = Occupation.order(:name)
    @states = State.order(:name)
  end

  def nationalities
    @nationalities ||= Nationality.order(:name)
  end
  helper_method :nationalities


  def item_params
    perms = params.require(:item).permit(permits)
    #perms.delete(:institution_id) if current_user.one_institution?
    perms
  end

end
