# frozen_string_literal: true
#
class PanelController < ApplicationController
  #acts_as_token_authentication_handler_for User

  before_filter :authenticate_user_from_token!

  before_action :authenticate_user!
  after_action  :set_csrf_cookie_for_ng
  #
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  private

  def authenticate_user_from_token!
    user_email = params[:user_email].presence
    user       = user_email && User.find_by(email: user_email)
    if user && Devise.secure_compare(user.authentication_token, params[:user_token])
      sign_out current_user if current_user
      sign_in user
      redirect_to url_for(params.permit(:anchor)) and return
    end
  end

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
end
