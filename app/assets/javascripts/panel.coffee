#= require jquery
#= require jquery_ujs
#= require jquery.remotipart
#= require foundation.min
#= require cocoon
#= require ckeditor/init
#= require angular
#= require angular-resource
#= require angular-animate
#= require ng-table
#= require checklist-model
#= require handsontable.full
#= require ngHandsontable
#= require moment
#= require moment-with-locales
#= require moment-range
#= require angular-mighty-datepicker
#= require fullcalendar.min
#= require panel/app
#= require dependent-fields-toggle
#= require panel/information_requests
#= require chosen.jquery.min
#= require jquery.validate
#= require jquery.timelinr
#= require jquery-ui
#= require jquery-ui/i18n/datepicker-es
#= require panel/information_requests_statistics
#= require tinymce

$ ->

  $(".alert-msg").click ->
    $(this).fadeOut 'slow', ->




  $('.input-tags').select2
    multiple: true

  $('.input-tokens').select2
    tags: true
    tokenSeparators: [',']
    placeholder: 'presupuesto,contrato,salud'
    createTag: (tag) ->
      {
        id: tag.term
        text: tag.term
        tag: true
      }
  .on 'select2:select', (evt) ->
    $.ajax
      type: 'POST'
      url: $(@).closest('form').attr('action')
      data: $(@).closest('form').serialize()
    return
  .on 'select2:unselect', (evt) ->
    $.ajax
      type: 'POST'
      url: $(@).closest('form').attr('action')
      data: $(@).closest('form').serialize()
    return


  $('.datepicker-statistic').datepicker
    changeMonth: true
    changeYear: true
    dateFormat: "yy-mm-dd"
    defaultDate: Date.parse($(this).val())
    $.datepicker.regional['es']



  $("#not-admit-form").validate()
  $("#correct-form").validate()
  $("#process-form").validate()
  $("#not-correct-form").validate()

  @dropdown = $ "#item_state_id"
  @target = $ "#item_city_id"

  @target
    .find("option")
    .remove()

  unless @dropdown.val() == ""
    @url = "//solicitudes.gobiernoabierto.gob.sv/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"
    #@url = "//localhost:3000/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"

    ($("<option />").text('-- Cualquiera --').val('')).appendTo(@target)

    $.getJSON @url, @extras, (data) =>
      $.each(data, (i, val) =>
        $("<option />")
          .val(i)
          .text(val)
          .appendTo(@target)
      )


  $(".ajaxable").on 'change', ->
    @dropdown = $ "#item_state_id"
    @target = $ "#item_city_id"

    @target
      .find("option")
      .remove()


    unless @dropdown.val() == ""
      @url = "//solicitudes.gobiernoabierto.gob.sv/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"
      #@url = "//localhost:3000/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"

      ($("<option />").text('-- Cualquiera --').val('')).appendTo(@target)

      $.getJSON @url, @extras, (data) =>
        $.each(data, (i, val) =>
          $("<option />")
            .val(i)
            .text(val)
            .appendTo(@target)
        )



  $('.open-side-form').each ->
    id = $(this).attr("index-id")
    $this = $(this)
    # assuming that the button is the next sibling
    $this.on 'click', (e) ->
      $("#requirement-content-"+id).toggleClass("medium-23 medium-12")
      $("#requirement-form-"+id).toggleClass("collapsible collapsed")
      $(".chosen").chosen()

      e.preventDefault()
      # to prevent page from jumping to the top of the page
      return
    return



  $('[data-xhrpopup]').on 'ajax:complete', (event, xhr, settings) ->
    html = $(xhr.responseText).hide()
    $('body').append(html).addClass('overlayed')
    html.fadeIn 'fast'

    $('[data-close]').on 'click', (e) ->
      e.preventDefault()
      $(this).closest('.overlay').fadeOut 'fast', ->
        $(this).remove()
        $('.overlayed').removeClass('overlayed')


  acc = $(".accordion")
  i = undefined
  i = 0
  while i < acc.length

    acc[i].onclick = ->

      ### Toggle between adding and removing the "active" class,
      to highlight the button that controls the panel
      ###

      @classList.toggle 'active'

      ### Toggle between hiding and showing the active panel ###

      panel = @nextElementSibling
      if panel.style.display == 'block'
        panel.style.display = 'none'
      else
        panel.style.display = 'block'
      return

    i++
