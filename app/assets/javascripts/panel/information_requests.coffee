# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$ ->
  new DependentFieldsToggle("input[name='item[residence]']", "other", ".residence_dependent", "hide")
  new DependentFieldsToggle("input[name='item[entity_type]']", "legal", ".entity_type_dependent", "hide")
  new DependentFieldsToggle("input[name='item[entity_type]']", "natural", ".entity_type_legal_dependent", "hide")
  new DependentFieldsToggle("input[name='item[delivery_way]']", "email", ".delivery_way_dependent", "show")
  new DependentFieldsToggle("input[name='item[reception]']", "in_person", ".reception_dependent", "show")
  ###
  $("#information_request_institution_id").on "change", ->
    $("#doesnt-online").remove()
    if $(this).val() != "" && $(this).find("option:selected").attr("accepts_online_requests") == "false"
      url = "http://api.gobiernoabierto.gob.sv/institutions/#{$(this).val()}/information_officer"
      #url = "http://api.localhost.com:3000/institutions/#{$(this).val()}/information_officer"
      select = $(this)
      $.getJSON url + "?callback=?", (data) =>
        $("<div />")
          .attr("id", "doesnt-online")
          .addClass("alert alert-info")
          .text("#{select.find("option:selected").text()} no acepta solicitudes de información a través del formulario en línea de Gobierno Abierto. Para realizar una solicitud de información por favor comuníquese con:")
          .append($("<br />"))
          .append($("<br />"))
          .append($("<h4 />").text(data.name))
          .append($("<span />").text(data.address))
          .append($("<br />"))
          .append($("<span />").text(data.email))
          .append($("<br />"))
          .append($("<span />").text(data.phone))
          .insertAfter(select.closest(".control-group"))

        $(".control-group").each (index, control_group) ->
          if $(control_group)[0] != select.closest(".control-group")[0]
            $(control_group).fadeOut()

        $("fieldset").each (index, fieldset) ->
          if $(fieldset)[0] != select.closest("fieldset")[0]
            $(fieldset).fadeOut()

        $(".form-actions").fadeOut()
    else
      $("fieldset:not(:visible), .control-group:not(:visible), .form-actions:not(:visible)").fadeIn()
  ###
  $('#information_request_salvadorian').on "change", ->
    $('#item_nationality_name').closest('.nationality-container').hide()
    $('#item_nationality_name').val('')

  $('#information_request_foreign').on "change", ->
    $('#item_nationality_name').closest('.nationality-container').show()

  $('.not-admitted-resolution-type').hide()

  $('select#information_type').on "change", ->
    optionSelected = $(this).find('option:selected').val()
    if(optionSelected=="unreasonable")
      $('.not-admitted-resolution-type').show()
    else
      $('.not-admitted-resolution-type').hide()

  $('#dont-admit').on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.not-admitted-form').toggle()

  $('#correct-request').on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.correct-form').toggle()

  $("#process-request").on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.process-form').toggle()

  $("#extended-request").on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.extended-form').toggle()

  $("#not-corrected-request").on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.not-corrected-form').toggle()

  $("#advanced-export").on "click", (e) ->
    e.stopPropagation()
    e.preventDefault()
    $('.advanced-export-div').toggle()

  $('#from_history').click ->
    if $(this).is(':checked') and !$('.history-date').is(':visible') or !$(this).is(':checked') and $('.history-date').is(':visible')
      $('.history-date').slideToggle()
    return
  $('#from_system').click ->
    if $(this).is(':checked') and !$('.system-date').is(':visible') or !$(this).is(':checked') and $('.system-date').is(':visible')
      $('.system-date').slideToggle()
    return

  #IAIP process form

  if $("#item_resolution_phase").is(':checked')
    $("#fullfillment_date").show()
  else
    $("#fullfillment_date").hide()

  $('#item_resolution_phase').on "change", ->
    if $("#item_resolution_phase").is(':checked')
      $("#fullfillment_date").show()
    else
      $("#fullfillment_date").hide()
    return
