google.load("visualization", "1", {packages:["corechart"]});
function drawPie3dChart(title, points, element) {
  var data = google.visualization.arrayToDataTable(points);
  var options = {
    title: title,
    pieHole: 0.4
  };
  var chart = new google.visualization.PieChart(document.getElementById(element));
  chart.draw(data, options);
}
function drawLineChart(title, points, element) {
  var data = google.visualization.arrayToDataTable(points);

  var options = {
    title: title
  };

  var chart = new google.visualization.LineChart(document.getElementById(element));
  chart.draw(data, options);
}
