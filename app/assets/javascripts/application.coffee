#= require jquery
#= require jquery_ujs
#= require jquery.remotipart
#= require foundation.min
#= require cocoon
#= require angular
#= require angular-resource
#= require angular-animate
#= require ng-table
#= require checklist-model
#= require handsontable.full
#= require ngHandsontable
#= require moment
#= require moment-with-locales
#= require moment-range
#= require angular-mighty-datepicker
#= require fullcalendar.min
#= require panel/app
#= require dependent-fields-toggle
#= require panel/information_requests
#= require chosen.jquery.min
#= require jquery.validate
#= require jquery.timelinr
#= require jquery-ui
#= require jquery-ui/i18n/datepicker-es
#= require tinymce

$ ->

  $('nav a').on 'click', (e) ->
    e.stopPropagation() unless $(this).hasClass('active')

  $('nav').on 'click', (e) ->
    if $(this).find('a:hidden').length > 0 || $(this).hasClass('active')
      e.preventDefault()
      $(this).toggleClass('active')
      $('body').toggleClass('overlayed')

  @dropdown = $ "#item_state_id"
  @target = $ "#item_city_id"

  @target
    .find("option")
    .remove()

  unless @dropdown.val() == ""
    @url = "//solicitudes.gobiernoabierto.gob.sv/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"
    #@url = "//localhost:3000/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"

    ($("<option />").text('-- Cualquiera --').val('')).appendTo(@target)

    $.getJSON @url, @extras, (data) =>
      $.each(data, (i, val) =>
        $("<option />")
          .val(i)
          .text(val)
          .appendTo(@target)
      )


  $(".ajaxable").on 'change', ->
    @dropdown = $ "#item_state_id"
    @target = $ "#item_city_id"

    @target
      .find("option")
      .remove()


    unless @dropdown.val() == ""
      @url = "//solicitudes.gobiernoabierto.gob.sv/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"
      #@url = "//localhost:3000/api/dropdowns/#{@dropdown.val()}/#{@dropdown.attr('action')}"

      ($("<option />").text('-- Cualquiera --').val('')).appendTo(@target)

      $.getJSON @url, @extras, (data) =>
        $.each(data, (i, val) =>
          $("<option />")
            .val(i)
            .text(val)
            .appendTo(@target)
        )
