#!/bin/env ruby
# encoding: utf-8

class UserMailer < ActionMailer::Base
  include Rails.application.routes.url_helpers

  default from: 'Info <info@gobiernoabierto.gob.sv>',
          to: 'Info <info@gobiernoabierto.gob.sv>'#,

  def new_information_request(information_request)

    if information_request.document_front_image.present?
      attachments[information_request.readable_document_image_filename(:front)] = File.read(information_request.document_front_image.path)
    end

    if information_request.document_back_image.present?
      attachments[information_request.readable_document_image_filename(:back)] = File.read(information_request.document_back_image.path)
    end

    @information_request = information_request
    recipient = (information_request.institution.nil? ? 'info@gobiernoabierto.gob.sv' : information_request.institution.officer_email)

    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
    end

    mail(
      to: recipient,
      from: information_request.email,
      reply_to: information_request.email,
      subject: 'Solicitud de información - GA'
    )
  end

  def new_personal_information_request(information_request)

    if information_request.document_front_image.present?
      attachments[information_request.readable_document_image_filename(:front)] = File.read(information_request.document_front_image.path)
    end

    if information_request.document_back_image.present?
      attachments[information_request.readable_document_image_filename(:back)] = File.read(information_request.document_back_image.path)
    end

    @information_request = information_request
    recipient = (information_request.institution.nil? ? 'info@gobiernoabierto.gob.sv' : information_request.institution.officer_email)

    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
    end

    mail(
      to: recipient,
      from: information_request.email,
      reply_to: information_request.email,
      subject: 'Solicitud de información - GA'
    )
  end

  def information_request_email_confirm(information_request)
    @information_request = information_request
    recipient = information_request.email
    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
    end
    mail(
      to: recipient,
      reply_to: information_request.email,

      subject: 'Confirme su email - Solicitud de Información'
    )
  end

  def personal_information_request_email_confirm(information_request)
    @information_request = information_request
    recipient = information_request.email
    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
    end
    mail(
      to: recipient,
      reply_to: information_request.email,

      subject: 'Confirme su email - Solicitud de Información de datos personales'
    )
  end

  def new_information_request_user_notification(information_request)
    @information_request = information_request
    recipient = information_request.email
    officer_email = (information_request.institution.nil? ? nil : information_request.institution.officer_email)
    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
      officer_email = 'oscar@gobiernoabierto.gob.sv'
    end
    mail(
      to: recipient,
      cc: officer_email,
      subject: 'Datos importantes - Solicitud de Información'
    )
  end

  def new_information_request_user_create(information_request)
    @information_request = information_request
    recipient = information_request.email
    if Rails.env.development?
      recipient = 'oscar@gobiernoabierto.gob.sv'
    end
    mail(
      to: recipient,
      subject: 'Solicitud ingresada con éxito - Solicitud de Información'
    )
  end

  def new_information_request_comment_to_user(current_user, requirement, user)

    @user = user
    @requirement = requirement
    @information_request = requirement.information_request
    @official = current_user
    message_info = {
      to: @user.email,
      from: @official.email,
      reply_to: @official.email,
      subject: "Requerimiento del oficial de información #{@official.name}"
    }
    if Rails.env.development?
      message_info[:to] = 'oscar@gobiernoabierto.gob.sv'
    end

    mail message_info
  end

  def new_information_request_comment_invite_user(current_user, requirement, user)
    @requirement = requirement
    @information_request = requirement.information_request
    @official = current_user
    @user = user
    message_info = {
      to: @user.email,
      from: @official.email,
      reply_to: @official.email,
      subject: "(Invitacion) Requerimiento del oficial de información #{@official.name}"
    }
    if Rails.env.development?
      message_info[:to] = 'oscar@gobiernoabierto.gob.sv'
    end
    mail message_info
  end

  def new_information_request_reply_to_comment(current_user, comment, requirement, user)
    @user = user
    @comment = comment
    @requirement = requirement
    @information_request = requirement.information_request
    @requirement_officer = requirement.user.email rescue @information_request.institution.officer_email
    message_info = {
      from: comment.user.email,
      to: @user.email,
      reply_to: @requirement_officer,
      subject: "Comentario en la solicitud #{@information_request.correlative}"
    }

    mail message_info
  end


  def information_request_report_spta
    message_info = {
      to: ['solicitudes@gobiernoabierto.gob.sv'],
      cc: ['oscar@gobiernoabierto.gob.sv'],
      subject: "Reporte SGS #{Date.current.strftime('%d/%m/%Y')}"
    }
    if Rails.env.development?
      message_info[:to] = 'oscar@gobiernoabierto.gob.sv'
      message_info[:cc] = 'oscar@gobiernoabierto.gob.sv'
    end
    mail message_info
  end

  def information_request_report_capres_spta
    message_info = {
      to: ['solicitudes@gobiernoabierto.gob.sv'],
      cc: ['oscar@gobiernoabierto.gob.sv'],
      subject: "Reporte CAPRES SGS #{Date.current.strftime('%d/%m/%Y')}"
    }
    if Rails.env.development?
      message_info[:to] = 'oscar@gobiernoabierto.gob.sv'
      message_info[:cc] = 'oscar@gobiernoabierto.gob.sv'
    end
    mail message_info
  end



end
