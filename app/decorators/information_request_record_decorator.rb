class InformationRequestRecordDecorator < ApplicationDecorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

end
