class InformationRequestIaipProcessDecorator < ApplicationDecorator
  delegate_all

  def process_type
    return '' if object.process_type.nil?
    h.t object.process_type,
      scope: 'activerecord.enum.information_request_iaip_process.process_type'
  end

  def resolution_result
    return '' if object.resolution_result.nil?
    h.t object.resolution_result,
      scope: 'activerecord.enum.information_request_iaip_process.resolution_result'
  end

  def resolution_phase
    return '' if object.resolution_phase.nil?
    object.resolution_phase == true ? 'Si' : 'No'
  end

  def judicial_process
    return '' if object.judicial_process.nil?
    object.judicial_process == true ? 'Si' : 'No'
  end

end
