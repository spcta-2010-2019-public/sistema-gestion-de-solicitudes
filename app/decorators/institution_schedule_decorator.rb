# frozen_string_literal: true
#
class InstitutionScheduleDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def wday
    h.t('date.day_names')[object.wday].capitalize
  end

  def from
    object.from.strftime('%I:%M%p')
  end

  def to
    object.to.strftime('%I:%M%p')
  end
end
