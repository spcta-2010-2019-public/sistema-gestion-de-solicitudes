# frozen_string_literal: true
#
class InstitutionCalendarDecorator < Draper::Decorator
  delegate_all

  def institution_calendar_id
    return '' if object.institution_calendar_id.nil?
    object.institution_calendar.name
  end

  def user_id
    return '' if object.user_id.nil?
    object.user.name
  end

end
