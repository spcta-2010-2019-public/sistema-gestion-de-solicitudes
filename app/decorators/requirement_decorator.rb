class RequirementDecorator < Draper::Decorator
  delegate_all

  def resolution_type
    return '' if object.resolution_type.nil?
    h.t object.resolution_type,
      scope: 'activerecord.enum.information_request.resolution_type'
  end
end
