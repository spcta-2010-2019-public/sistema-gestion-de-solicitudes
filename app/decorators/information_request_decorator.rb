class InformationRequestDecorator < ApplicationDecorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def occupation_id
    return '' if object.occupation.nil?
    object.occupation.name
  end

  def city_id
    return '' if object.city.nil?
    object.city.name
  end

  def nationality
    return '' if object.nationality.nil?
    h.t object.nationality,
      scope: 'activerecord.enum.information_request.nationality'
  end

  def residence
    return '' if object.residence.nil?
    h.t object.residence,
      scope: 'activerecord.enum.information_request.residence'
  end

  def state_sym
    return '' if object.state.nil?
    object.state
  end

  def entity_type
    return '' if object.entity_type.nil?
    h.t object.entity_type,
      scope: 'activerecord.enum.information_request.entity_type'
  end

  def state
    return '' if object.state.nil?
    h.t object.state,
      scope: 'activerecord.enum.information_request.state'
  end

  def document_type
    return '' if object.document_type.nil?
    h.t object.document_type,
      scope: 'activerecord.enum.information_request.document_type'
  end

  def notification_mode
    return '' if object.notification_mode.nil?
    h.t object.notification_mode,
      scope: 'activerecord.enum.information_request.notification_mode'
  end

  def type_of_rights
    return '' if object.type_of_rights.nil?
    h.t object.type_of_rights,
      scope: 'activerecord.enum.information_request.type_of_rights'
  end


  def educational_level
    return '' if object.educational_level.nil?
    h.t object.educational_level,
      scope: 'activerecord.enum.information_request.educational_level'
  end

  def gender
    return '' if object.gender.nil?
    h.t object.gender,
      scope: 'activerecord.enum.information_request.gender'
  end

  def delivery_way
    return '' if object.delivery_way.nil?
    h.t object.delivery_way,
      scope: 'activerecord.enum.information_request.delivery_way'
  end

  def iaip_process_kind
    return '' if object.iaip_process_kind.nil?
    h.t object.iaip_process_kind,
      scope: 'activerecord.enum.information_request.iaip_process_kind'
  end

end
