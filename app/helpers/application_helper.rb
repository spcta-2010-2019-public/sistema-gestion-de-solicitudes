module ApplicationHelper
  def display_errors errors
    if errors
      content_tag(:div, class: 'alert alert-danger') do
        content_tag(:ul) do
          errors.each do |msg|
            concat content_tag(:li, msg)
          end
        end
      end
    end
  end

  def display_error_for attr
    if @errors && @errors[attr]
      content_tag(:span, class: 'has-error') do
        concat @errors[attr].join(', ')
      end
    end
  end

  def flash_messages
    flash.map do |k, v|
      content_tag(:div, v, class: "alert #{k == 'notice' ? 'success' : 'warning'}")
    end.join.html_safe
  end
end
