namespace :information_requests do
  desc 'Set confirmed at to confirmed requests'
  task :load => :environment do
    InformationRequest.where(confirmed: true).all.each do |i|
      i.update_column(:confirmed_at, i.created_at) unless i.confirmed_at.present?
    end
  end

  namespace :reports do
    desc 'Send reports to SPTA'
    task :ssta => :environment do
      if InformationRequest.confirmed.where(confirmed_at: 1.day.ago.beginning_of_day .. Date.current.beginning_of_day).count > 0
        UserMailer.information_request_report_spta.deliver
      end
    end
    desc 'Send capres reports to SPTA'
    task :capres => :environment do
      if InformationRequest.confirmed.where(confirmed_at: 1.week.ago.beginning_of_day .. Date.current.beginning_of_day, institution_id: 9).count > 0
        UserMailer.information_request_report_capres_spta.deliver
      end
    end
  end
end
