require 'paperclip'
#
ActiveRecord::Base.configurations = Rails.application.config.database_configuration
#
# ###
# # CONSTANTS
# ###
old_root = '/home/www/vhosts/gobiernoabierto/current'
#
# ###
# # RAKE PROCESS
# ###
namespace :migration do
  desc "Migrate data from Old Site"
  task :run => :environment do

    ###
    # Migrate InstitutionTypes
    ###

    puts "========== INSTITUTION TYPE =========="
    OldInstitutionType.all.each do |it|
      begin
        o = InstitutionType.where(id: it.id).first_or_initialize
        if o.new_record?
          o.name  = it.name
          o.enabled  = 1
          o.priority  = 0
          o.created_at = it.created_at || Time.current
          o.updated_at = it.updated_at || Time.current
          unless o.save(validate: false)
            log_message o.errors.full_messages
          end
        end
      rescue Exception => e
        log_message e
      end
    end

    ###
    # Migrate institutions
    ###
    puts "========== INSTITUTION =========="
    OldInstitution.find_in_batches do |institutions|
      institutions.each do |institution|
        if institution.institution_type_id != 5 || institution.institution_type_id != 6
          begin
            i = Institution.where(id: institution.id).first_or_initialize
            if i.new_record?
              #Institution is new, we create it
              officer = OldInformationOfficer.where(institution_id: institution.id).first
              i.name      = institution.name
              i.acronym   = institution.acronym
              #i.avatar    = institution.logo_path_file(old_root) ? File.open(institution.logo_path_file(old_root)) : nil
              i.institution_type_id   = institution.institution_type_id
              if officer
                i.officer_email = officer.email
                i.officer_name  = officer.name
              end
              unless i.save(validate: false)
                puts log_message i.errors.full_messages
              end
            end
          rescue Exception => e
            log_message e
          end
        end
      end
    end

    # Migrate oir addresses
    ###
    puts "========== INFORMATION OFFICER =========="
    OldInformationOfficer.find_in_batches do |officers|
     officers.each do |ofc|
       begin
         address = OirAddress.where(institution_id: ofc.institution_id).first_or_initialize
         if address.new_record?
           address.enabled = true
           address.lat = ofc.latitude
           address.lng = ofc.longitude
           address.phone = ofc.phone
           address.address = ofc.address
           unless address.save(validate: false)
             log_message address.errors.full_messages
           end
         end
       rescue Exception => e
         log_message e
       end
     end
    end

    ###
    # Migrate officers users
    ###
    puts "========== USERS =========="
    OldInstitution.order(:id).each do |ins|
      officer = OldInformationOfficer.where(institution_id: ins.id).first
      if officer
        user    = OldUser.where(email: officer.email).first
        ins_new = Institution.where(id: ins.id).where.not(institution_type_id: [5,6]).first
        if user and ins_new
          encrypted_password  = user.encrypted_password
          a       = User.where(email: user.email).first_or_initialize
          a.name  = user.name
          a.email = user.email
          a.password  = user.email
          if a.save(validation: false)
            a.add_role 'oir' if a.roles.blank?
            a.institution_ids = [ins_new.id]
            a.update_attribute(:encrypted_password, encrypted_password)
          else
            puts log_message "Usuario no se guardo: #{user.id} - #{a.errors.full_messages}"
          end
        end
      end
    end
    ###
    # Correct id sequences
    ###
    ActiveRecord::Base.connection.tables.each do |table|
      begin
        ActiveRecord::Base.connection.execute("SELECT setval('#{table}_id_seq', COALESCE((SELECT MAX(id)+1 FROM #{table}), 1), false);")
      rescue
        # do nothing
      end
    end

  end

  desc "Get next correlative from institutions"
  task :enable_institutions => :environment do
    puts "========== DISABLING ALL INSTITUTIONS =========="
    institutions = Institution.all
    institutions.update_all(enabled: false)

    old_institutions = OldInstitution.where(accepts_online_information_requests: true)

    institutions = Institution.where(acronym: old_institutions.map(&:acronym))
    puts "========== ENABLING INSTITUTIONS WITH ONLINE REQUESTS =========="
    if institutions.size > 0
      institutions.each do |institution|
        old_institution = OldInstitution.where(acronym: institution.acronym).first
        institution.enabled = true
        institution.information_request_correlative = old_institution.try(:information_request_correlative)
        if institution.save
        else
          institution.errors.full_messages
        end
      end
    end
  end

  desc "Migrate users from given params"
  task :custom_users => :environment do
    puts "========== USERS =========="
    users_array = {}
    if users_array.size > 0
      users_array.each do |node|
        user = OldUser.where(email: node[0]).first
        if user
          encrypted_password  = user.encrypted_password
          a       = User.where(email: user.email).first_or_initialize
          a.name  = user.name
          a.email = user.email
          a.password  = user.email
          if a.save(validation: false)
            a.add_role 'oir' if a.roles.blank?
            a.institution_ids = [node[1]]
            a.update_attribute(:encrypted_password, encrypted_password)
          else
            puts log_message "Usuario no se guardo: #{user.id} - #{a.errors.full_messages}"
          end
        end
      end
    end

  end
end

#
# ###
# # LOG METHOD
# ###
#
def log_message message
  open("#{Rails.root.to_s}/log/migration.log", 'a') do |f|
    f.puts ""
    f.puts "-----------------------------"
    f.puts "#{message}"
  end
end
#
#
###
# DEFINE OLD CLASS
###

class OldUser < ActiveRecord::Base
  self.abstract_class = true
  establish_connection 'old_version'.to_sym
  self.table_name = 'users'
end
#
#
class OldInstitutionType < ActiveRecord::Base
  self.abstract_class = true
  establish_connection 'old_version'.to_sym
  self.table_name = 'institution_types'
end

class OldInstitution < ActiveRecord::Base
  include Paperclip::Glue
  self.abstract_class = true
  establish_connection 'old_version'.to_sym
  self.table_name = 'institutions'

  has_attached_file :logo
  def logo_path_file old_root
    @logo_path_file ||= logo.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_institutions', 'institutions')
  end
end

class OldInformationOfficer < ActiveRecord::Base
  include Paperclip::Glue
  self.abstract_class = true
  establish_connection 'old_version'.to_sym
  self.table_name = 'information_officers'
end
