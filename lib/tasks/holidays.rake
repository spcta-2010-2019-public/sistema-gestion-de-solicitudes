namespace :holidays do
  desc "Set holiday dates to institutions"
  task :add_dates => :environment do
    puts "============ADDING HOLIDAYS============="
    Institution.where.not(institution_type_id: [5,6]).each do |institution|
      dates_array = [Date.parse("2018-11-02"), Date.parse("2018-12-22"), Date.parse("2018-12-23"),
        Date.parse("2018-12-24"), Date.parse("2018-12-25"), Date.parse("2018-12-26"),
        Date.parse("2018-12-27"), Date.parse("2018-12-28"), Date.parse("2018-12-29"),
        Date.parse("2018-12-30"), Date.parse("2018-12-31"), Date.parse("2019-01-01"),
        Date.parse("2018-01-02")]
      institution_calendar = institution.institution_calendar
      if institution_calendar and dates_array
        dates_array.each do |node|
          new_event = CalendarEvent.where(event_date: node, institution_calendar_id: institution_calendar.id).first_or_initialize
          new_event.institution_calendar_id = institution_calendar.id
          new_event.name = "Feriado"
          new_event.kind = :holiday
          new_event.event_date = node
          unless new_event.save!
            log_message new_event.errors.full_messages
          end
        end
      end
    end

  end

end
