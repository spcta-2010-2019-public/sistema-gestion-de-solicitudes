Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: { sessions: 'users/sessions' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'panel/home#index'


  %w(404 422 500 503).each do |code|
    get code, to: 'errors#show', code: code
  end


  resources :information_requests, only: [:index] do
    collection do
      get 'confirm'
    end
  end

  namespace :panel do
    root to: 'home#index'

    resources :profile, only: [:edit, :update]
    resources :versions, only: [:show]

    resources :admin_settings, only: [] do
      put :table_columns, on: :collection
    end

    resources :institution_calendars, only: [:index, :show, :new] do
      resources :calendar_events, only: [:new, :edit]
    end

    resources :information_requests do
      member do
        get 'admitted'
        get 'not_admitted'
        put 'process_request'
        get 'closure'
        put 'close_request'
      end
      collection do
        put 'updates'
        put 'destroys'
        get 'list'
        get 'get_states'
        get 'statistics'
        get 'basic_csv_export'
        get 'advanced_csv_export'
      end

    end

    resources :requirements do
      member do
        put 'management'
      end
    end

    resources :comments

    [
      :institutions,
      :oir_addresses,
      :users,
      :occupations,
      :events,
      :institution_schedules,
      :information_request_records
    ].each do |resource|
      resources resource do
        put :updates, on: :collection
        put :destroys, on: :collection
        get :list, on: :collection
      end
    end
  end

  namespace :api do
    resources :dropdowns do
      member do
        get :state_cities
      end
    end
  end


end
