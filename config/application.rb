require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Sgs
  class Application < Rails::Application
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = 'Central America'
    config.i18n.default_locale = :es
    config.active_job.queue_adapter = :delayed_job
    config.autoload_paths << Rails.root.join('app').join('pdf')
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end
  end
end
Config = YAML.load_file("#{Rails.root}/config/config.yml").with_indifferent_access
