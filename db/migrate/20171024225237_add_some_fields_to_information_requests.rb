class AddSomeFieldsToInformationRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :information_requests, :meets_shape, :boolean
    add_column :information_requests, :tags, :string
    add_column :information_requests, :legal_representative, :string
    add_attachment :information_requests, :legal_representative_doc
  end
end
