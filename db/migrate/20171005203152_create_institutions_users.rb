class CreateInstitutionsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :institutions_users do |t|
      t.references :user, foreign_key: true
      t.references :institution, foreign_key: true
    end
  end
end
