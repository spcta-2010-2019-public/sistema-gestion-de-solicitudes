class RemoveIndexRequirementsOnAssignedAndInvitedUsersFromDatabase < ActiveRecord::Migration[5.0]

  def up
    execute <<-SQL
      DROP INDEX IF EXISTS index_requirements_on_assigned_users_and_invited_users;
    SQL
  end

  def down
    execute <<-SQL
      CREATE UNIQUE INDEX index_requirements_on_assigned_users_and_invited_users ON requirements USING btree (sort_array(assigned_users), sort_array(invited_users));
    SQL
  end
end
