class AddSomeFieldsToRequirements < ActiveRecord::Migration[5.0]
  def change
    add_column :requirements, :meets_depth, :boolean
    add_column :requirements, :art_74_laip, :boolean
  end
end
