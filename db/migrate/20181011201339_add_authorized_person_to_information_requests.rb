class AddAuthorizedPersonToInformationRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :information_requests, :authorized_person, :string
    add_column :information_requests, :iaip_process_kind, :integer
  end
end
