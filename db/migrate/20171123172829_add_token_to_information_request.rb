class AddTokenToInformationRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :information_requests, :token, :text
  end
end
