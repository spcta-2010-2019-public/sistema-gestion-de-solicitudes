class AddFieldsToInformationRequestIaipProcess < ActiveRecord::Migration[5.0]
  def change
    add_column :information_request_iaip_processes, :iaip_resolution, :integer
    add_column :information_request_iaip_processes, :judicial_process, :boolean, default: false
  end
end
