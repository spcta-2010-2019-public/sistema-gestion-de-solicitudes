class CreateInformationRequestIaipProcesses < ActiveRecord::Migration[5.0]
  def change
    create_table :information_request_iaip_processes do |t|
      t.references :information_request, index: {name: "information_request_id"}
      t.date :presentation_date
      t.date :notification_date
      t.string :correlative
      t.integer :process_type
      t.text :description
      t.integer :resolution_result
      t.boolean :resolution_phase, default: false
      t.date :fullfillment_date
      t.text :comments
      t.timestamps
    end
  end
end
