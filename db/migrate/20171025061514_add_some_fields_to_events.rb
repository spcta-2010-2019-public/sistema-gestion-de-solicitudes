class AddSomeFieldsToEvents < ActiveRecord::Migration[5.0]
  def change
    add_reference :events, :eventable, polymorphic: true
    add_reference :events, :event_id
    add_column :events, :event_start, :datetime
  end
end
