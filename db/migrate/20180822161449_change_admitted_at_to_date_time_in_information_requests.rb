class ChangeAdmittedAtToDateTimeInInformationRequests < ActiveRecord::Migration[5.0]
  def up
    change_column :information_requests, :admitted_at, :datetime
  end

  def down
    change_column :information_requests, :admitted_at, :date
  end
end
