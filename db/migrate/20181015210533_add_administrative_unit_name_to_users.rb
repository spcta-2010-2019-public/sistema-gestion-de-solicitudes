class AddAdministrativeUnitNameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :administrative_unit_name, :string
  end
end
