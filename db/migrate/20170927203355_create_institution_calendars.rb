class CreateInstitutionCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_calendars do |t|
      t.references :institution
      t.string :name
      t.integer :year
      t.timestamps
    end
  end
end
