class ChangeCalendarEventsDates < ActiveRecord::Migration[5.0]
  def change
    remove_column :calendar_events, :start_at, :datetime
    remove_column :calendar_events, :end_at, :datetime
    add_column :calendar_events, :event_date, :date
  end
end
