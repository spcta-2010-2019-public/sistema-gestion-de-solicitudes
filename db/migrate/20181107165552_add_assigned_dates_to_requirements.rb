class AddAssignedDatesToRequirements < ActiveRecord::Migration[5.0]
  def change
    add_column :requirements, :assigned_date, :datetime
    add_column :requirements, :assigned_date_update, :datetime
  end
end
