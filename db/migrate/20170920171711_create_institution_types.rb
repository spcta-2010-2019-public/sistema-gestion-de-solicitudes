class CreateInstitutionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_types do |t|
      t.boolean :enabled, null: false, default: false
      t.integer :priority, null: false, default: 0
      t.string :name, null: false, default: ''
      t.timestamps
    end
  end
end
