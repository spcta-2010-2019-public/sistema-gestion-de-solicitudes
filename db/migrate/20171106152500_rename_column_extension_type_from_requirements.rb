class RenameColumnExtensionTypeFromRequirements < ActiveRecord::Migration[5.0]
  def change
    rename_column :requirements, :extension_type, :information_type
  end
end
