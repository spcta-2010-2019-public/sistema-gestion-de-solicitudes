class ChangeAdmittedAtTypeFromInformationRequest < ActiveRecord::Migration[5.0]
  def change
    change_column :information_requests, :admitted_at, :datetime
  end
end
