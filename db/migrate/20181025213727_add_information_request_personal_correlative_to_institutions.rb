class AddInformationRequestPersonalCorrelativeToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :enabled, :boolean, default: true
    add_column :institutions, :information_request_personal_correlative, :integer
  end
end
