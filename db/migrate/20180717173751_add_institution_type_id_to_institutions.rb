class AddInstitutionTypeIdToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :institution_type_id, :integer
  end
end
