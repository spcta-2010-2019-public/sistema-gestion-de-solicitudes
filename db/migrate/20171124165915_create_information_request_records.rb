class CreateInformationRequestRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :information_request_records do |t|
      t.date :opening_date
      t.date :closing_date
      t.belongs_to :institution
      t.belongs_to :user
      t.integer :total_requests, default: 0
      t.integer :total_requirements, default: 0
      t.integer :dismissed_requirements, default: 0
      t.integer :abandoned_requirements, default: 0
      t.integer :art_10_17_laip, default: 0
      t.integer :no_proprietary, default: 0
      t.integer :art_31_32_laip, default: 0
      t.integer :art_30_laip, default: 0
      t.integer :art_30_laip_reserved, default: 0
      t.integer :art_30_laip_confidential, default: 0
      t.integer :art_19_laip, default: 0
      t.integer :art_24_laip, default: 0
      t.integer :art_73_laip, default: 0
      t.integer :art_74_laip, default: 0
      t.integer :art_67_laip, default: 0
      t.integer :process_requirements, default: 0
      t.integer :other, default: 0
      t.timestamps
    end
  end

end
