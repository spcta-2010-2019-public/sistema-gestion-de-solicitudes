class AddFunctionArrayToDatabase < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      CREATE FUNCTION sort_array(unsorted_array anyarray) RETURNS anyarray AS $$
        BEGIN
          RETURN (SELECT ARRAY_AGG(val) AS sorted_array
          FROM (SELECT UNNEST(unsorted_array) AS val ORDER BY val) AS sorted_vals);
        END;
      $$ LANGUAGE plpgsql IMMUTABLE STRICT;

      CREATE UNIQUE INDEX index_requirements_on_assigned_users_and_invited_users ON requirements USING btree (sort_array(assigned_users), sort_array(invited_users));
    SQL
  end

  def down
    execute <<-SQL
      DROP INDEX IF EXISTS index_requirements_on_assigned_users_and_invited_users;
      DROP FUNCTION IF EXISTS sort_array(unsorted_array anyarray);
    SQL
  end
end
