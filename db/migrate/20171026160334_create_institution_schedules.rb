class CreateInstitutionSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_schedules do |t|
      t.references :institution, foreign_key: true
      t.integer :wday
      t.time :from
      t.time :to

      t.timestamps
    end
  end
end
