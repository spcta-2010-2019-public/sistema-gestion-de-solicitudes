class CreateInformationRequestIaipProcessNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :information_request_iaip_process_notes do |t|
      t.references :information_request_iaip_process, index: {:name => "iaip_process"}
      t.date :note_date
      t.text :content
      t.timestamps
    end
  end
end
