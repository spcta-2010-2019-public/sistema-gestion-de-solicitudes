class CreateInformationRequestIaipProcessAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :information_request_iaip_process_attachments do |t|
      t.references :information_request_iaip_process, index: {:name => "iaip_process_attachment"}
      t.string :title
      t.text :description
      t.attachment :file
      t.timestamps
    end
  end
end
