class ChangeColumnTypeFromRequirements < ActiveRecord::Migration[5.0]
  def change
    remove_column :requirements, :assigned_users, :string
    remove_column :requirements, :invited_users, :string
    add_column :requirements, :assigned_users, :text, array: true, default: []
    add_column :requirements, :invited_users, :text, array: true, default: []
  end
end
