class AddPersonalDataRequestFieldsToInformationRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :information_requests, :type_of_rights, :string
    add_column :information_requests, :request_type, :integer
  end
end
