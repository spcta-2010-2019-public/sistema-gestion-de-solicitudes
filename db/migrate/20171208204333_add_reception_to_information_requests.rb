class AddReceptionToInformationRequests < ActiveRecord::Migration[5.0]
  def change
    add_column :information_requests, :reception, :integer
  end
end
