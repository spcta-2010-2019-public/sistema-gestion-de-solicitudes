class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.references :user
      t.text :justification
      t.string :state
      t.attachment :document
      t.timestamps
    end
  end
end
