class AddRequirementIdToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :requirement_id, :integer
  end
end
